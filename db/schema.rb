# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_16_063118) do

  create_table "academic_year_notifications", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "title", limit: 200, null: false
    t.text "desc", size: :medium
    t.bigint "academic_year_id", null: false, unsigned: true
    t.bigint "teacher_id", null: false, unsigned: true
    t.boolean "is_public", default: false, null: false
    t.string "filename1", limit: 150
    t.string "filename2", limit: 150
    t.string "filename3", limit: 150
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["title"], name: "title_idx"
  end

  create_table "academic_year_periods", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "academic_year_id", unsigned: true
    t.string "period_title", limit: 100
    t.text "period_desc"
    t.date "period_start"
    t.date "period_end"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "academic_years", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "description", limit: 200
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "app_settings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "kk", limit: 150, null: false
    t.string "vv", limit: 150, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["kk"], name: "kk_unique", unique: true
  end

  create_table "classroom_schedules", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "course_id", null: false, unsigned: true
    t.bigint "classroom_id", null: false, unsigned: true
    t.string "schedule_information", limit: 50
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["classroom_id", "course_id"], name: "schedule_unique", unique: true
  end

  create_table "classrooms", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "title", limit: 40, null: false
    t.string "description", limit: 300
    t.bigint "academic_year_id", unsigned: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["title"], name: "title_idx"
  end

  create_table "classrooms_courses", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "classroom_id", null: false
    t.bigint "course_id", null: false
    t.index ["course_id", "classroom_id"], name: "course_classroom"
  end

  create_table "classrooms_users", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "classroom_id", null: false
    t.bigint "user_id", null: false
    t.index ["user_id", "classroom_id"], name: "user_classroom"
  end

  create_table "courses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.boolean "is_pre_kinder", default: true
    t.boolean "is_kinder", default: true
    t.boolean "is_middle_school", default: true
    t.boolean "is_high_school", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "name_idx", unique: true
  end

  create_table "homework_options", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "desc", limit: 300, null: false
    t.boolean "valid_answer", default: false, null: false
    t.bigint "homework_id", null: false, unsigned: true
    t.integer "option_order", null: false, unsigned: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "homeworks", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "title", limit: 200, null: false
    t.text "content"
    t.bigint "course_id", null: false, unsigned: true
    t.bigint "classroom_id", null: false, unsigned: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "due_date"
    t.boolean "active", default: true
    t.string "folder", limit: 100
    t.string "filename1", limit: 150
    t.string "filename2", limit: 150
    t.string "filename3", limit: 150
    t.integer "of_type", default: 1, null: false, unsigned: true
    t.string "filename4", limit: 150
    t.string "filename5", limit: 150
    t.bigint "period_id", unsigned: true
    t.boolean "will_be_evaluated", default: false
  end

  create_table "parents", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "fullname", limit: 200, null: false
    t.string "document_number", limit: 30
    t.string "parent_type", limit: 30
    t.date "onomastic"
    t.string "phone_number1", limit: 30
    t.string "phone_number2", limit: 30
    t.string "email1", limit: 50
    t.string "email2", limit: 50
    t.string "address1", limit: 300
    t.string "address2", limit: 300
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["document_number"], name: "document_number_unique", unique: true
    t.index ["fullname"], name: "fullname_index"
    t.index ["parent_type"], name: "parent_type_idx"
  end

  create_table "parents_users", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "parent_id", null: false
    t.index ["user_id", "parent_id"], name: "user_parent"
  end

  create_table "student_homeworks", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "student_id", null: false, unsigned: true
    t.bigint "homework_id", null: false, unsigned: true
    t.decimal "rating", precision: 4, scale: 2
    t.text "details"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id", "homework_id"], name: "student_homework_unique", unique: true
  end

  create_table "teacher_course_classrooms", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "teacher_id", null: false
    t.bigint "course_id", null: false
    t.bigint "classroom_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["teacher_id", "course_id", "classroom_id"], name: "teacher_course_classroom_unique", unique: true
  end

  create_table "user_history_requests", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "user_id", unsigned: true
    t.text "json"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "user_tokens", primary_key: "token", id: :string, limit: 200, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "device_type", null: false
    t.boolean "active", default: true, null: false
    t.bigint "user_id", null: false, unsigned: true
    t.string "ip", limit: 50, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["token"], name: "token_index"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "username", limit: 50, null: false
    t.string "email1", limit: 50
    t.string "email2", limit: 50
    t.string "phone_number1", limit: 30
    t.string "phone_number2", limit: 30
    t.string "fullname", limit: 200, null: false
    t.boolean "active", default: true
    t.boolean "sex", default: true, null: false
    t.boolean "user_type", default: true, null: false
    t.string "password_digest", limit: 200, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "onomastic"
    t.boolean "is_teacher_admin", default: false, null: false
    t.string "picture_filename", limit: 200
    t.string "document_number", limit: 30
    t.index ["email1"], name: "email1_idx"
    t.index ["email2"], name: "email2_idx"
    t.index ["fullname"], name: "fullname_idx"
    t.index ["phone_number1"], name: "phone_number1_idx"
    t.index ["phone_number2"], name: "phone_number2_idx"
    t.index ["username"], name: "username_unique", unique: true
  end

end
