# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

[
{
  id: 1,
  username: ENV.fetch('USER_TEST_USERNAME') , 
  fullname: ENV.fetch('USER_TEST_FULLNAME'),
  document_number: ENV.fetch('USER_TEST_DOCUMENT_NUMBER'),
  sex: true,
  user_type: false,
  is_teacher_admin: true,
  password: ENV.fetch('USER_TEST_PASSWORD') , 
  password_confirmation: ENV.fetch('USER_TEST_PASSWORD') , 
}, 
].each do |user|
  u = User.new user
  u.save!(validate: false)
end if !User.any?

[
  {
    id: 1,
    kk: 'school_name',
    vv: "Mi colegio",
  },
  {
    id: 2,
    kk: 'school_motto',
    vv: 'Lema del colegio o del año',
  },
  {
    id: 3,
    kk: 'school_logo',
    vv: 'logo.png',
  },
  {
    id: 4,
    kk: 'school_address',
    vv: '',
  },
  {
    id: 5,
    kk: 'school_business_name',
    vv: '',
  },
  {
    id: 6,
    kk: 'school_phone_number1',
    vv: '',
  },
  {
    id: 7,
    kk: 'school_phone_number2',
    vv: '',
  },
  {
    id: 8,
    kk: 'school_email1',
    vv: '',
  },
  {
    id: 9,
    kk: 'school_email2',
    vv: '',
  },
  {
    id: 10,
    kk: 'school_facebook_url',
    vv: '',
  },
  {
    id: 11,
    kk: 'school_youtube_url',
    vv: '',
  },
  {
    id: 12,
    kk: 'school_instagram_url',
    vv: '',
  },
  {
    id: 13,
    kk: 'school_website_title',
    vv: '',
  },
  {
    id: 14,
    kk: 'school_website_description',
    vv: '',
  },
  {
    id: 15,
    kk: 'school_website_keywords',
    vv: '',
  },
  {
    id: 16,
    kk: 'school_website_theme_color',
    vv: '#DF8C03',
  },
  {
    id: 17,
    kk: 'school_login_title',
    vv: 'Login título',
  },
  {
    id: 18,
    kk: 'school_login_header_title',
    vv: 'Intranet - Virtual Classroom',
  },
  {
    id: 19,
    kk: 'school_login_header_color_title',
    vv: '#f00',
  },
  {
    id: 20,
    kk: 'school_login_header_background_color_title',
    vv: '#f00',
  },
].each do |json|
  record = AppSetting.new json
  record.save!(validate: false)
end if !AppSetting.any?