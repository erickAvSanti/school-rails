class AddFolderAndFilesToHomeworks < ActiveRecord::Migration[6.0]
  def change
    add_column :homeworks, :folder, :string, limit: 100, null: true, default: nil
    add_column :homeworks, :filename1, :string, limit: 150, null: true, default: nil
    add_column :homeworks, :filename2, :string, limit: 150, null: true, default: nil
    add_column :homeworks, :filename3, :string, limit: 150, null: true, default: nil
  end
end
