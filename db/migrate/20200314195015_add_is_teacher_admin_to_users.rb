class AddIsTeacherAdminToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :is_teacher_admin, :boolean, null: false, default: false
  end
end
