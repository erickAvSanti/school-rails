class AddDueDateToHomeworks < ActiveRecord::Migration[6.0]
  def change
    add_column :homeworks, :due_date, :datetime
  end
end
