class CreateUserTokens < ActiveRecord::Migration[6.0]
  def change
    create_table :user_tokens, id: false  do |t|
      t.string :token, limit: 190, null: false, index: {name: :token_index}, primary_key: true
      t.boolean :device_type, null: false, comments: "true => desktop, false => mobile"
      t.boolean :active, null: false, default: true
      t.bigint :user_id, null: false, unsigned: true
      t.string :ip, null: false, limit: 50
      t.timestamps
    end
  end
end
