class CreateHomeworks < ActiveRecord::Migration[6.0]
  def change
    create_table :homeworks do |t|
      t.string :title, limit: 190, null: false
      t.text :content
      t.bigint :course_id, unsigned: true, null: false
      t.bigint :classroom_id, unsigned: true, null: false
      t.timestamps
    end
  end
end
