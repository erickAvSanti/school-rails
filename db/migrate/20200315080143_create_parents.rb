class CreateParents < ActiveRecord::Migration[6.0]
  def change
    create_table :parents do |t|
      t.string :fullname, limit: 190, null: false, index: {name: :fullname_index, unique: false}
      t.string :document_number, limit: 30, null: true, index: {name: :document_number_unique, unique: true}
      t.string :parent_type, limit: 30, null: true, index: {name: :parent_type_idx, unique: false}
      t.date :onomastic
      t.string :phone_number1, limit: 30, null: true
      t.string :phone_number2, limit: 30, null: true
      t.string :email1, limit: 50, null: true
      t.string :email2, limit: 50, null: true
      t.text :address1
      t.text :address2
      t.timestamps
    end
  end
end
