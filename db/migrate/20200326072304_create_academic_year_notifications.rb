class CreateAcademicYearNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :academic_year_notifications do |t|
      t.string :title, limit: 190, null: false, index: {name: :title_idx, unique: false}
      t.text :desc
      t.bigint :academic_year_id, unsigned: true, null: false
      t.bigint :teacher_id, unsigned: true, null: false
      t.boolean :is_public, null: false, default: false
      t.string :filename1, limit: 150, null: true, default: nil 
      t.string :filename2, limit: 150, null: true, default: nil 
      t.string :filename3, limit: 150, null: true, default: nil 
      t.timestamps
    end
  end
end
