class CreateJoinTableClassroomStudent < ActiveRecord::Migration[6.0]
  def change
    create_join_table :classrooms, :users do |t|
      t.index [:user_id, :classroom_id], name: :user_classroom
    end
  end
end
