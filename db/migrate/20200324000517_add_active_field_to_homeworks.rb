class AddActiveFieldToHomeworks < ActiveRecord::Migration[6.0]
  def change
    add_column :homeworks, :active, :boolean, default: true
  end
end
