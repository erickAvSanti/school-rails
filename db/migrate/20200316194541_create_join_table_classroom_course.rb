class CreateJoinTableClassroomCourse < ActiveRecord::Migration[6.0]
  def change
    create_join_table :classrooms, :courses do |t|
      t.index [:course_id, :classroom_id], name: :course_classroom
    end
  end
end
