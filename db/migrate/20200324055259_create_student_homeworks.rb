class CreateStudentHomeworks < ActiveRecord::Migration[6.0]
  def change
    create_table :student_homeworks do |t|
      t.bigint :student_id, unsigned: true, null: false
      t.bigint :homework_id, unsigned: true, null: false
      t.decimal :rating, precision: 4, scale: 2
      t.text :details
      t.timestamps
    end

    add_index :student_homeworks, [:student_id, :homework_id], unique: true, name: :student_homework_unique

  end
end
