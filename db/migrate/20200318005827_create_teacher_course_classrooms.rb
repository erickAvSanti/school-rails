class CreateTeacherCourseClassrooms < ActiveRecord::Migration[6.0]
  def change
    create_table :teacher_course_classrooms do |t|
      t.bigint :teacher_id, null: false, unsigned: true
      t.bigint :course_id, null: false, unsigned: true
      t.bigint :classroom_id, null: false, unsigned: true
      t.timestamps
    end
    add_index :teacher_course_classrooms, [:teacher_id, :course_id, :classroom_id], unique: true, name: :teacher_course_classroom_unique
  end
end
