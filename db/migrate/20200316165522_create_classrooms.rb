class CreateClassrooms < ActiveRecord::Migration[6.0]
  def change
    create_table :classrooms do |t|
      t.string :title, limit: 40, null: false, index: {name: :title_idx, unique: false}
      t.string :description, limit: 300, null: true
      t.bigint :academic_year_id, unsigned: true
      t.timestamps
    end
  end
end
