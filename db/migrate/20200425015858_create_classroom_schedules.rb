class CreateClassroomSchedules < ActiveRecord::Migration[6.0]
  def change
    create_table :classroom_schedules do |t|
      t.bigint :course_id, null: false, unsigned: true
      t.bigint :classroom_id, null: false, unsigned: true
      t.string :schedule_information, limit: 50, null: true
      t.timestamps
    end
    add_index :classroom_schedules, [:classroom_id, :course_id], unique: true, name: :schedule_unique
  end
end
