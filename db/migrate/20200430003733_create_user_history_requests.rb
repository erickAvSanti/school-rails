class CreateUserHistoryRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :user_history_requests do |t|
      t.bigint :user_id, unsigned: true
      t.text :json
      t.timestamps
    end
  end
end
