class AddPictureFilenameToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :picture_filename,:string, limit: 200, index: {name: :picture_filename_unique, unique: true}
  end
end
