class AddTypeToHomeworks < ActiveRecord::Migration[6.0]
  def change
    add_column :homeworks, :of_type, :integer, unsigned: true, null: false, default: 1, comments: "1=>tarea, 2=>observación, 3=> examen, 4=>calificación"
  end
end
