class CreateJoinTableUserParent < ActiveRecord::Migration[6.0]
  def change
    create_join_table :users, :parents do |t|
      t.index [:user_id, :parent_id], name: :user_parent
    end
  end
end
