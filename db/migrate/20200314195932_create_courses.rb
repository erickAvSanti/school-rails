class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string :name, null: false, limit: 100, index: {name: :name_idx, unique: true}
      t.boolean :is_pre_kinder, default: true
      t.boolean :is_kinder, default: true
      t.boolean :is_middle_school, default: true
      t.boolean :is_high_school, default: true
      t.timestamps
    end
  end
end
