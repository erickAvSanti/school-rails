class CreateHomeworkOptions < ActiveRecord::Migration[6.0]
  def change
    create_table :homework_options do |t|
      t.string :desc, limit: 190, null: false
      t.boolean :valid_answer, null: false, default: false
      t.bigint :homework_id, null: false, unsigned: true
      t.integer :option_order, null: false, unsigned: true
      t.timestamps
    end
  end
end
