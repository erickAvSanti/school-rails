class AddDocumentNumberToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :document_number, :string, limit: 30, null: true, index: {name: :document_number_unique, unique: true}
  end
end
