class CreateAcademicYears < ActiveRecord::Migration[6.0]
  def change
    create_table :academic_years do |t|
      t.string :description, limit: 190, null: true
      t.timestamps
    end
  end
end
