class CreateAppSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :app_settings do |t|
      t.string :kk, limit: 150, null: false, index: {name: :kk_unique, unique: true}
      t.string :vv, limit: 150, null: false
      t.timestamps
    end
  end
end
