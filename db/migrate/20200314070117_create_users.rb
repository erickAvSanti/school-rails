class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :username, null: false,limit: 50, index: {unique: true, name:'username_unique'}
      t.string :email1, null: true, limit: 50, index: {unique: false, name: 'email1_idx'}
      t.string :email2, null: true, limit: 50, index: {unique: false, name: 'email2_idx'}
      t.string :phone_number1, null: true, limit: 30, index: {unique: false, name: 'phone_number1_idx'}
      t.string :phone_number2, null: true, limit: 30, index: {unique: false, name: 'phone_number2_idx'}
      t.string :fullname, null: false, limit: 190, index: {unique: false, name: 'fullname_idx'}
      t.boolean :active, default: true
      t.boolean :sex, null: false, default: true, comments: "true => male, false => female"
      t.boolean :user_type, null: false, default: true, comments: "true => kinder, false => teacher"
      t.string :password_digest, null: false, limit: 190
      t.timestamps
    end
  end
end
