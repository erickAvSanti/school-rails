class AddExtraFilesToHomeworks < ActiveRecord::Migration[6.0]
  def change
    add_column :homeworks, :filename4, :string, limit: 150, null: true, default: nil
    add_column :homeworks, :filename5, :string, limit: 150, null: true, default: nil
  end
end
