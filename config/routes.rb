Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  class DomainConstraint
    def matches?(request)
      ( request.host.start_with?( ENV['RAILS_DOMAIN_CONSTRAINT'] ) or request.host.start_with?( "www.#{ENV['RAILS_DOMAIN_CONSTRAINT']}" ) )
    end
  end
  class SubDomainConstraint
    def matches?(request)
      request.host.start_with?( ENV['RAILS_SUB_DOMAIN_CONSTRAINT'] )
    end
  end

  constraints( DomainConstraint.new )  do
    get '/', to: 'website#index'
  end
  constraints( SubDomainConstraint.new ) do
    get '/', to: 'intranet#index'
    scope '/users' do
      get "/to_options", to: "user#to_options"
      get "/sessions", to: "user#list_sessions"
      get "/", to: "user#list"
      post "/", to: "user#create"
      delete '/:id', to: 'user#delete'
      put '/:id', to: 'user#update'
    end


    scope '/courses' do
      scope  '/for-teachers' do 
        get "/", to: "courses#for_teachers_list"
        get "/classroom-course-info", to: "courses#for_teachers_classroom_course_info"
        post "/", to: "courses#for_teachers_create"
        post "/set-schedule", to: "courses#set_schedule"
        delete '/:id', to: 'courses#for_teachers_delete'
        put '/:id', to: 'courses#for_teachers_update'
        scope '/homeworks' do 
          get '/', to: "courses#homeworks"
          post '/', to: "courses#homework_create"
          post '/set-ratings', to: "courses#homeworks_set_ratings"
          put '/:id', to: "courses#homework_update"
          delete '/:id', to: "courses#homework_delete"
        end
      end
    end

    scope '/students' do
      get "/full-profile", to: "students#full_profile"
      get "/homeworks", to: "homeworks#get_homeworks"
      scope  '/for-teachers' do 
        get "/", to: "students#for_teachers_list"
        get "/:id", to: "students#for_teachers_get_info"
        post "/", to: "students#for_teachers_create"
        delete '/:id', to: 'students#for_teachers_delete'
        put '/:id', to: 'students#for_teachers_update'
      end
      get "/notifications/:id", to: "academic_year_notifications#get_student_notifications"
    end

    scope '/teachers' do
      get "/full-profile", to: "teachers#full_profile"
      scope  '/for-teachers' do 
        get "/", to: "teachers#for_teachers_list"
        get "/:id", to: "teachers#for_teachers_get_info"
        post "/", to: "teachers#for_teachers_create"
        delete '/:id', to: 'teachers#for_teachers_delete'
        put '/:id', to: 'teachers#for_teachers_update'
      end
    end

    scope '/settings' do
      scope  '/for-teachers' do 
        get "/", to: "app_settings#list"
        put "/:id", to: "app_settings#update", :constraints => { :id => /\d+/ }
        put "/login-header-title", to: "app_settings#update_login_header_title"
        put "/website-theme-color", to: "app_settings#update_website_theme_color"
      end
    end

    scope '/parents' do
      scope  '/for-teachers' do 
        get "/find-parents-for-student", to: "parents#find_parents_for_student"
        put "/set-parents-for-student/:id", to: "parents#set_parents_for_student"
        get "/", to: "parents#for_teachers_list"
        get "/:id", to: "parents#for_teachers_get_info"
        post "/", to: "parents#for_teachers_create"
        post '/modify-parents-data', to: 'parents#modify_parents_data'
        delete '/:id', to: 'parents#for_teachers_delete'
        put '/:id', to: 'parents#for_teachers_update'
        put '/create-new-parent-for/:id', to: 'parents#create_new_parent_for_student'
        put '/destroy-parent-for/:id', to: 'parents#destroy_parent_for'
        put '/remove-parent-for/:id', to: 'parents#remove_parent_for'
      end
    end

    scope '/academic-years' do
      scope '/notifications' do 
        scope 'for-teachers' do 
          get "/:id", to: "academic_year_notifications#for_teachers_get_notifications"
          post "/", to: "academic_year_notifications#for_teachers_create"
          put "/:id", to: "academic_year_notifications#for_teachers_update"
          delete "/:id", to: "academic_year_notifications#for_teachers_delete"
        end
      end
      scope  '/for-teachers' do 
        get "/", to: "academic_years#for_teachers_list"
        get "/:id", to: "academic_years#for_teachers_get_info"
        post "/", to: "academic_years#for_teachers_create"
        delete '/:id', to: 'academic_years#for_teachers_delete'
        put '/:id', to: 'academic_years#for_teachers_update'
        scope '/classrooms' do 
          get "/:id", to: "academic_years#for_teachers_classrooms_list"
          post "/", to: "academic_years#for_teachers_classrooms_create"
          delete '/:id', to: 'academic_years#for_teachers_classrooms_delete'
          put '/:id', to: 'academic_years#for_teachers_classrooms_update'
          put '/assign-students/:id', to: 'academic_years#for_teachers_classrooms_assign_students'
          put '/assign-courses/:id', to: 'academic_years#for_teachers_classrooms_assign_courses'
          put '/assign-teachers/:id', to: 'academic_years#for_teachers_classrooms_assign_teachers'
        end
        scope '/students' do 
          get "/find", to: "academic_years#for_teachers_find_student"
          get "/:id", to: "academic_years#for_teachers_get_students_from_classroom"
        end
        scope '/courses' do 
          get "/find", to: "academic_years#for_teachers_find_course"
          get "/:id", to: "academic_years#for_teachers_get_courses_from_classroom"
        end
        scope '/teachers' do 
          get "/find", to: "academic_years#for_teachers_find_teacher"
          get "/:id", to: "academic_years#for_teachers_get_courses_from_classroom"
        end
      end
    end

    get 'basic-profile', to: 'users#basic_profile'

    post 'authenticate', to: 'authentication#authenticate'
    post 'logout', to: 'authentication#logout'

    class NoEndingFiles
      def matches?(request) 
        (request.fullpath =~ /\.\w+(\?([\s\S]+))?$/).blank? && (request.original_url.include?( ENV['RAILS_SUB_DOMAIN_CONSTRAINT'] ) ).present?
      end
    end
    match '*match' , to: 'intranet#index', constraints: NoEndingFiles.new, via: :get

  end
end
