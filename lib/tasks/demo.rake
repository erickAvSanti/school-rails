namespace :demo do
  task :seed_courses => :environment do 
    CoursesHelper.generate_seed
    CoursesHelper.create_data_from_seed_generated
  end
  task :seed_students => :environment do 
    StudentsHelper.generate_seed
    StudentsHelper.create_data_from_seed_generated
  end
  task :seed_parents => :environment do 
    ParentsHelper.generate_seed
    ParentsHelper.create_data_from_seed_generated
  end
  task :seed_teachers => :environment do 
    TeachersHelper.generate_seed
    TeachersHelper.create_data_from_seed_generated
  end
  task :seed_classrooms => :environment do 
    AcademicYearsHelper.generate_seed_classrooms
    AcademicYearsHelper.create_data_from_seed_generated_classrooms
  end
  task :seed_classrooms_students => :environment do 
    AcademicYearsHelper.generate_seed_classrooms_students
  end
  task :seed_classrooms_courses => :environment do 
    AcademicYearsHelper.generate_seed_classrooms_courses
  end
  task :seed_classrooms_courses_teachers => :environment do 
    AcademicYearsHelper.generate_seed_classrooms_courses_teachers
  end
  task :seed_classrooms_courses_homeworks => :environment do 
    AcademicYearsHelper.generate_seed_classrooms_courses_homeworks
  end
  task :clear_classroom_course_homeworks => :environment do 
    DemoHelper.clearAppData
  end
end
