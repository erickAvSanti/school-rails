namespace :website do
  task :convert_images_front_to_thumb => :environment do 
    files = Dir.glob("#{Rails.root}/app/assets/images/front/*.jpg")
    files.each do |file|
      next if file =~ /thumb_/
      image = MiniMagick::Image.open(file)
      puts File.basename(file)
      dim = image.dimensions
      width = dim[0]
      height = dim[1]
      orientation = ( width < height ) ? 'v' : 'h'
      size = ( width < height ) ? width : height
      if orientation == 'h'
        image.crop "#{size}x#{size}+#{(width - size)/2}+0"
      else
        image.crop "#{size}x#{size}+0+#{(height - size)/2}"
      end
      image.resize "200x200"
      thumbnail = file.gsub(/(\w+\.\w+)$/,"thumb_\\1")
      image.write(thumbnail)
    end
  end
end