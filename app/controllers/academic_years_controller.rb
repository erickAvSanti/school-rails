class AcademicYearsController < ApplicationController

  before_action :verify_if_user_is_teacher

  before_action :verify_if_user_teacher_can_create_update_delete, only: [
    :for_teachers_create,
    :for_teachers_update,
    :for_teachers_delete,
    :for_teachers_classrooms_create,
    :for_teachers_classrooms_update,
    :for_teachers_classrooms_delete,
    :for_teachers_classrooms_assign_students,
    :for_teachers_classrooms_assign_courses,
  ]




  def for_teachers_list
    order_by = '' 
    order_orientation = false
    id                = params[:id] if params[:id].present?
    order_by          = params[:order_by] if params[:order_by].present?
    order_orientation = params[:order_orientation]=='true' if params[:order_orientation].present?
    page              = params.has_key?(:page) ? params[:page].to_i : 1
    regs_x_page       = params.has_key?(:regs_x_page) ? params[:regs_x_page].to_i : REGS_X_PAGE
    records = []
    builder = AcademicYear
    if order_by == 'year'
      builder = builder.order("academic_years.id #{ order_orientation ? 'ASC' : 'DESC' }")
    end

    total = builder.count
    total_pages = ( total.to_f / regs_x_page.to_f ).floor + ( total % regs_x_page > 0 ? 1 : 0 )
    page = page > total_pages ? total_pages : page
    if total_pages>0
      offset = (page - 1) * regs_x_page
      builder.limit(regs_x_page).offset(offset).each do |record|
        obj = record.attributes
        records << obj
      end
    else
      page = 1
      total_pages = 0
    end
    render json: { 
      values: records, 
      total_records: total, 
      current_page: page, 
      total_pages: total_pages, 
      regs_x_page: regs_x_page, 
      order_by: order_by,
      order_orientation: order_orientation,  
    } , status: :ok
  end

  def for_teachers_get_info
    begin
      record = AcademicYear.find params[:id]
      json = record.attributes
      render json: json, status: :ok
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end
  def for_teachers_create
    begin
      pp = for_academic_years_valid_params
      record = AcademicYear.new pp
      record.id = params[:id].to_i
      record.save!
      render json:{data: record}, status: :ok
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end

  def for_teachers_update
    begin
      record = AcademicYear.find params[:id]
      pp = for_academic_years_valid_params
      record.update! pp
      render json:record, status: :ok
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end

  def for_teachers_delete
    puts "delete params #{params}"
    begin
      record = AcademicYear.find params[:id]
      record.destroy!
      if record.id != 1
        render json: {}, status: :ok
      else
        render json: {}, status: :not_acceptable
      end
    rescue => e
      puts "error => #{e.message}"
      render json: {}, status: :not_found
    end
  end

  def for_teachers_classrooms_create
    begin
      record = Classroom.new for_academic_years_classroom_valid_params
      record.save!
      render json:{data: record}, status: :ok
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end

  def for_teachers_classrooms_update
    begin
      record = Classroom.find params[:id]
      pp = for_academic_years_classroom_valid_params
      pp.each do |k,v|
        pp[k].upcase! if v.kind_of? String
      end
      record.update! pp
      render json:record, status: :ok
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end

  def for_teachers_classrooms_list
    begin

      if not @current_user.is_teacher_admin?
        records = Classroom.distinct
          .joins("JOIN classrooms_courses ON classrooms_courses.classroom_id = classrooms.id")
          .joins("JOIN courses ON classrooms_courses.course_id = courses.id")
          .joins("JOIN teacher_course_classrooms ON ( teacher_course_classrooms.course_id = courses.id AND teacher_course_classrooms.classroom_id = classrooms.id )")
          .where(" classrooms.academic_year_id = #{params[:id]} ")
          .where(" teacher_course_classrooms.teacher_id = #{@current_user.id} ")
          .order(" classrooms.title ASC ")
      else
        records = Classroom.where(academic_year_id: params[:id].to_i).order(title: :asc)
      end
      data = []
      records.each do |record|
        obj = record.attributes
        obj['students'] = record.users.students.order(fullname: :asc)
        courses_with_teachers = record.courses_with_teachers_and_total_homeworks
        #courses_with_teachers = record.courses_with_teachers
        unique_teachers = []
        courses_with_teachers.each do |cc|
          unique_teachers += cc['teachers']
        end
        unique_teachers.uniq! { |tt| tt.id }
        obj['courses'] = courses_with_teachers
        obj['unique_teachers'] = unique_teachers
        data << obj
      end
      render json:{values: data}, status: :ok
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end

  def for_teachers_classrooms_delete
    puts "delete params #{params}"
    begin
      record = Classroom.find params[:id]
      record.destroy!
      render json: {}, status: :ok
    rescue => e
      puts "error => #{e.message}"
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_find_student
    word = params[:word]
    if word.present?
      records = User.students.where("fullname LIKE ?","%#{word}%").order(fullname: :asc)
      render json:{ values: records }, status: :ok
    else
      render json:{ values: [] }, status: :bad_request
    end
  end

  def for_teachers_classrooms_assign_students
    begin
      id = params[:id]
      ids = params[:ids]
      ActiveRecord::Base.transaction do
        record = Classroom.find id
        record.set_students_with_ids ids
        render json: {values: record.users}, status: :ok
      end
    rescue => e
      puts e.message
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_get_students_from_classroom
    begin
      id = params[:id]
      classroom = Classroom.find id
      render json: {values: classroom.users.students.order(fullname: :asc)}, status: :ok
    rescue => e
      puts e
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_find_course
    word = params[:word]
    if word.present?
      records = Course.where("name LIKE ?","%#{word}%").order(name: :asc)
      render json:{ values: records }, status: :ok
    else
      render json:{ values: [] }, status: :bad_request
    end
  end

  def for_teachers_classrooms_assign_courses
    begin
      id = params[:id]
      ids = params[:ids]
      teachers = params[:teachers]
      ActiveRecord::Base.transaction do
        record = Classroom.find id
        record.set_courses_with_ids ids
        Classroom.set_teachers_for_courses(record,teachers)
        arr_courses = record.courses_with_teachers_and_total_homeworks
        
        render json: { values: arr_courses }, status: :ok
      end
    rescue => e
      puts e.message
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_get_courses_from_classroom
    begin
      id = params[:id]
      classroom = Classroom.find id
      render json: {values: classroom.courses_with_teachers_and_total_homeworks}, status: :ok
    rescue => e
      puts e
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_find_teacher
    word = params[:word]
    if word.present?
      records = User.teachers.where("fullname LIKE ?","%#{word}%").order(fullname: :asc)
      render json:{ values: records }, status: :ok
    else
      render json:{ values: [] }, status: :bad_request
    end
  end

  private

  def for_academic_years_valid_params

    params.permit(
      :id,
      :description,
    )
  end
  def for_academic_years_classroom_valid_params

    academic_year_id = params[:academic_year_id]
    AcademicYear.new_with_year(academic_year_id) if academic_year_id.present?
    params.permit(
      :id,
      :title,
      :description,
      :academic_year_id,
    )
  end
end
