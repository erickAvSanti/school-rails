class ParentsController < ApplicationController
  before_action :verify_if_user_is_teacher

  before_action :verify_if_user_teacher_can_create_update_delete, only: [
    :set_parents_for_student,
    :modify_parents_data,
    :create_new_parent_for_student,
    :destroy_parent_for,
    :remove_parent_for,
    :for_teachers_delete,
  ]

  def find_parents_for_student
    word = params[:word]
    if word.present?
      records = Parent.where("fullname LIKE ? ","%#{word}%")
      render json: { values: records }, status: :ok
    else
      render json: {msg: 'word undefined'}, status: :not_found
    end
  end
  def set_parents_for_student
    ids = params[:ids]
    record = User.find params[:id]
    values = Parent.where("id IN (?)", ids )
    ActiveRecord::Base.transaction do
      record.parents.clear
      record.parents << values if values.count > 0
      render json: {}, status: :ok
    end
  rescue => e
    puts e.message
    render json: {}, status: :bad_request
  end

  def modify_parents_data
    current_record = nil
    begin
      parents = params.require(:parents)
      if parents.present?
        ActiveRecord::Base.transaction do
          parents.each do |parent|
            current_record = nil
            puts parent.to_s
            parent = parent.permit(parent.keys).to_h
            parent.except!('updated_at','created_at')
            current_record = record = Parent.find parent['id']
            record.update! parent
          end
        end
        render json: {}, status: :ok
      else
        render json: {}, status: :bad_request
      end
    rescue => e
      puts e.message
      render json: ( current_record.present? ? { record: {id: current_record.id, fullname: current_record.fullname} } : {}), status: :unprocessable_entity
    end
  end

  def create_new_parent_for_student
    id = params[:id]
    parent = params.require(:parent)
    parent = Parent.clean_parameters parent
    ActiveRecord::Base.transaction do
      record = User.find id
      pp = Parent.new parent
      pp.save!
      record.parents << pp
      render json: pp, status: :ok
    end
  rescue => e
    puts e.class.to_s
    puts e.message
    if e.class == ActiveRecord::RecordNotUnique
      render json: {msg:'El número de documento u otro detalle ya existe'}, status: :unprocessable_entity
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  def destroy_parent_for
    id = params[:id].to_i
    parent_id = params[:parent_id].to_i
    ActiveRecord::Base.transaction do
      record = User.find id
      parent = Parent.find parent_id
      if parent.users.count == 1 and parent.users.first.id == id
        record.parents.find(parent_id).destroy
      else
        puts "No se puede eliminar #{id}, #{parent_id}"
      end
      render json: {}, status: :ok
    end
  rescue => e
    puts e.message
    render json: {}, status: :unprocessable_entity
  end

  def remove_parent_for
    id = params[:id]
    parent_id = params[:parent_id]
    ActiveRecord::Base.transaction do
      record = User.find id
      record.parents.delete parent_id
      render json: {}, status: :ok
    end
  rescue => e
    puts e.message
    render json: {}, status: :unprocessable_entity
  end

  def for_teachers_delete
    id = params[:id]
    pp = Parent.find id
    if not pp.users.any?
      pp.destroy
      render json: {}, status: :ok
    else
      render json: {msg: 'Este pariente tiene alumnos dependientes'}, status: :unprocessable_entity
    end
  rescue => e
    puts e.message
    render json: {}, status: :unprocessable_entity
  end
end
