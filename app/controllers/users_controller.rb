class UsersController < ApplicationController
  def basic_profile
    record = @current_user
    obj = record.attributes
    obj.delete 'password_digest'
    obj.delete 'is_teacher_admin'


    json = {}
    AppSetting.all.each do |record|
      json[record.kk] = record.vv
    end

    obj['settings'] = json
    render json: obj, status: :ok
  end
end
