class IntranetController < ApplicationController
  before_action :authenticate_request, except: [:index]
  def index
    school_logo = AppSetting.get_from_key( 'school_logo' )
    @prop = {
      'school_website_title' => AppSetting.get_from_key( 'school_name' ),
      'web_login_title' => AppSetting.get_from_key( 'school_login_title', 'Login title' ),
      'web_login_header_title' => AppSetting.get_from_key( 'school_login_header_title', 'Intranet - Virtual Classroom' ),
      'web_login_header_color_title' => AppSetting.get_from_key( 'school_login_header_color_title', '#fff' ),
      'web_login_header_background_color_title' => AppSetting.get_from_key( 'school_login_header_background_color_title', '#f00' ),
      'web_url' => request.original_url,
      'school_website_description' => AppSetting.get_from_key( 'school_website_description' ),
      'school_website_keywords' => AppSetting.get_from_key( 'school_website_keywords' ),
      'school_website_theme_color' => AppSetting.get_from_key( 'school_website_theme_color' ),
      'web_logo' => school_logo,
      'web_logo_type' => File.extname( Rails.root.join('public', school_logo ).to_s.split('?')[0] ).downcase.gsub(/\./, ''),
    }
    render template: "intranet/index.html.erb", layout: false
  end
end
