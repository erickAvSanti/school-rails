class AppSettingsController < ApplicationController
  def list
    render json: AppSetting.all, status: :ok
  end

  def update_login_header_title
    ActiveRecord::Base.transaction do
      header_color_title = params[:header_color_title]
      record = AppSetting.where(kk: 'school_login_header_color_title').take
      record.update!({vv: header_color_title})

      header_background_color_title = params[:header_background_color_title]
      record = AppSetting.where(kk: 'school_login_header_background_color_title').take
      record.update!({vv: header_background_color_title})
    end
    render json: {msg: 'success'}, status: :ok
  rescue => e
    puts e.message
    Rails.logger.info e.message
    render json: nil, status: :bad_request
  end

  def update_website_theme_color

    website_theme_color = params[:website_theme_color]
    record = AppSetting.where(kk: 'school_website_theme_color').take
    record.update!({vv: website_theme_color})
    
    render json: {msg: 'success'}, status: :ok
  rescue => e
    puts e.message
    Rails.logger.info e.message
    render json: nil, status: :bad_request
  end

  def update
    record = AppSetting.find params[:id]
    if params[:file].present?
      file = params[:file]
      img_ext = File.extname(file.tempfile.path).downcase
      raise "Invalid format" if img_ext != '.png'
      filename = "school_logo#{img_ext}"
      filename_path = Rails.root.join('public',filename)
      FileUtils.cp file.tempfile.path, filename_path
      if File.file? filename_path
        AppSetting.cropSquare(filename_path, nil, 400)
        AppSetting.makeFavicons(filename_path)
      end
      params[:vv] = "#{filename}?img=#{params[:vv]}"
      record.update! valid_params(false)
    else
      record.update! valid_params
    end
    render json: record, status: :ok
  rescue => e 
    puts e.backtrace.join("\n")
    Rails.logger.info e.backtrace.join("\n")
    render json: nil, status: :bad_request
  end

  def valid_params( cond = true )
    opt = [:kk,:vv,:id]
    if cond 
      params.require(:app_setting).permit(opt) 
    else
      params.permit(opt)
    end
  end
end
