class CoursesController < ApplicationController
	before_action :verify_if_user_is_teacher


	before_action :verify_if_user_teacher_can_create_update_delete, only: [
		:for_teachers_create,
		:for_teachers_update,
		:for_teachers_delete,
	]

	REGS_X_PAGE = 20
	REGS_X_PAGE_MAX = 200
	def for_teachers_list
		order_by = '' 
		order_orientation = false
		id                = params[:id] if params[:id].present?
		order_by          = params[:order_by] if params[:order_by].present?
		order_orientation = params[:order_orientation]=='true' if params[:order_orientation].present?
		page              = params.has_key?(:page) ? params[:page].to_i : 1
		regs_x_page       = params.has_key?(:regs_x_page) ? params[:regs_x_page].to_i : REGS_X_PAGE
		regs_x_page       = REGS_X_PAGE_MAX if regs_x_page > REGS_X_PAGE_MAX

		records = []
		builder = Course
		if order_by == 'name'
			builder = builder.order("courses.name #{ order_orientation ? 'ASC' : 'DESC' }")
		end

		total = builder.count
		total_pages = ( total.to_f / regs_x_page.to_f ).floor + ( total % regs_x_page > 0 ? 1 : 0 )
		page = page > total_pages ? total_pages : page
		if total_pages>0
			offset = (page - 1) * regs_x_page
			builder.limit(regs_x_page).offset(offset).each do |record|
				obj = record.attributes
				records << obj
			end
		else
			page = 1
			total_pages = 0
		end
		render json: { 
			values: records, 
			total_records: total, 
			current_page: page, 
			total_pages: total_pages, 
			regs_x_page: regs_x_page, 
			order_by: order_by,
			order_orientation: order_orientation,  
			im_admin: @current_user.is_teacher_admin
		} , status: :ok
	end

	def for_teachers_create
		begin
			record = Course.new valid_params
			record.save!
			render json: record, status: :ok
		rescue => e
			puts "error => #{e.message}"
			record = nil
			render json: record, status: :bad_request
		end
	end

	def for_teachers_update
		begin
			record = Course.find params[:id]
			record.update! valid_params
			render json: record, status: :ok
		rescue => e
			puts "error => #{e.message}"
			record = nil
			render json: record, status: :bad_request
		end
	end

	def for_teachers_delete
		puts "delete params #{params}"
		begin
			record = Course.find params[:id]
			record.destroy
			render json: {}, status: :ok
		rescue => e
			puts "error => #{e.message}"
			render json: {}, status: :not_found
		end
	end

	def set_schedule
		puts "set_schedule params #{params}"

		if not @current_user.is_teacher_admin? and not teacher_belongs_to_classroom_and_course? params[:course_id], params[:classroom_id]
			render json: {}, status: :forbidden
			return
		end
		
		record = ClassroomSchedule.set_for(params[:classroom_id], params[:course_id], params[:schedule_information])

		render json: {schedule: record}, status: :ok
	end

	def for_teachers_classroom_course_info
		puts "curren params = #{params}"
		begin
			classroom_id  = params[:classroom_id]
			course_id     = params[:course_id]

			classroom = Classroom.find classroom_id
			course    = classroom.courses.find course_id

			tcc = TeacherCourseClassroom.where(classroom_id: classroom.id, course_id: course.id)
			teachers = []
			tcc.each do |reg|
				teacher = User.teachers.where(id: reg.teacher_id).take
				teachers << teacher if teacher.present?
			end if tcc.present?
			render json: { 
				schedule: ClassroomSchedule.where(classroom_id: classroom.id, course_id: course.id).take,
				classroom: classroom, 
				course: course, 
				students: classroom.users.students,
				teachers: teachers 
			}, status: :ok
			
		rescue => e
			puts e.message
			render json:{}, status: :unprocessable_entity
		end
	end

	def homeworks
		begin

			order_by = '' 
			order_orientation = false
			id                = params[:id] if params[:id].present?
			of_type           = params[:of_type] if params[:of_type].present?
			order_by          = params[:order_by] if params[:order_by].present?
			order_orientation = params[:order_orientation]=='true' if params[:order_orientation].present?
			page              = params.has_key?(:page) ? params[:page].to_i : 1
			regs_x_page       = params.has_key?(:regs_x_page) ? params[:regs_x_page].to_i : REGS_X_PAGE
			regs_x_page       = REGS_X_PAGE_MAX if regs_x_page > REGS_X_PAGE_MAX

			records = []
			builder = Homework.where(classroom_id: params[:classroom_id], course_id: params[:course_id])
			builder = builder.where(of_type: of_type.to_i) if of_type.present?
			if order_by == 'homework_title'
				builder = builder.order("homeworks.title #{ order_orientation ? 'ASC' : 'DESC' }")
			end
			if order_by == 'homework_active'
				builder = builder.order("homeworks.active #{ order_orientation ? 'ASC' : 'DESC' }")
			end
			if order_by == 'homework_created_at'
				builder = builder.order("homeworks.created_at #{ order_orientation ? 'ASC' : 'DESC' }")
			end
			if order_by == 'homework_updated_at'
				builder = builder.order("homeworks.updated_at #{ order_orientation ? 'ASC' : 'DESC' }")
			end
			if order_by == 'homework_due_date'
				builder = builder.order("homeworks.due_date #{ order_orientation ? 'ASC' : 'DESC' }")
			end

			total = builder.count
			total_pages = ( total.to_f / regs_x_page.to_f ).floor + ( total % regs_x_page > 0 ? 1 : 0 )
			page = page > total_pages ? total_pages : page
			if total_pages>0
				offset = (page - 1) * regs_x_page
				builder.limit(regs_x_page).offset(offset).each do |record|
					obj = record.attributes

					obj['fs1'] = record.get_filename_size(1).to_f
					obj['fs2'] = record.get_filename_size(2).to_f
					obj['fs3'] = record.get_filename_size(3).to_f
					obj['fs4'] = record.get_filename_size(4).to_f
					obj['fs5'] = record.get_filename_size(5).to_f

					obj['rating'] = record.student_homeworks
					obj['options'] = record.homework_options.order(option_order: :asc)
					records << obj
				end
			else
				page = 1
				total_pages = 0
			end
			render json: { 
				related_courses: Classroom.find(params[:classroom_id]).courses.where("courses.id <> #{params[:course_id]}"),
				total_of_types: Homework.get_total_by_type(params[:classroom_id], params[:course_id]),
				values: records, 
				total_records: total, 
				current_page: page, 
				total_pages: total_pages, 
				regs_x_page: regs_x_page, 
				order_by: order_by,
				order_orientation: order_orientation,  
				im_admin: @current_user.is_teacher_admin,
				can_add_edit_del: teacher_belongs_to_classroom_and_course?(params[:course_id], params[:classroom_id]),
			} , status: :ok
		rescue => e
			puts e.message
			render json: {}, status: :unprocessable_entity
		end
	end

	def homework_create
		begin

			if not @current_user.is_teacher_admin? and not teacher_belongs_to_classroom_and_course? params[:course_id], params[:classroom_id]
				render json: {}, status: :forbidden
				return
			end

			puts params

			ActiveRecord::Base::transaction do 
				record = Homework.new valid_homework_params
				record.id = nil
				record.folder = DemoHelper.rand_string if record.folder.blank?
				record.save!

				Homework.set_filename(1, params[:filename1_file], record) if params[:filename1_file].present?
				Homework.set_filename(2, params[:filename2_file], record) if params[:filename2_file].present?
				Homework.set_filename(3, params[:filename3_file], record) if params[:filename3_file].present?
				Homework.set_filename(4, params[:filename4_file], record) if params[:filename4_file].present?
				Homework.set_filename(5, params[:filename5_file], record) if params[:filename5_file].present?

				record.verify_filenames
				record.set_options params[:options]

				json = record.attributes
				json['options'] = record.homework_options.order(option_order: :asc)

				render json: json, status: :ok
			end
		rescue => e
			puts e.message
			render json: {}, status: :unprocessable_entity
		end
	end

	def homework_update
		begin

			if not @current_user.is_teacher_admin? and not teacher_belongs_to_classroom_and_course? params[:course_id], params[:classroom_id]
				render json: {}, status: :forbidden
				return
			end
			ActiveRecord::Base.transaction do

				record = Homework.where(id: params[:id], course_id: params[:course_id], classroom_id: params[:classroom_id]).take
				record.folder = DemoHelper.rand_string if record.folder.blank?
				record.update! valid_homework_params

				record.remove_filename(1) if params[:filename1_delete] == "true" or params[:filename1_delete] == true
				record.remove_filename(2) if params[:filename2_delete] == "true" or params[:filename2_delete] == true
				record.remove_filename(3) if params[:filename3_delete] == "true" or params[:filename3_delete] == true
				record.remove_filename(4) if params[:filename4_delete] == "true" or params[:filename4_delete] == true
				record.remove_filename(5) if params[:filename5_delete] == "true" or params[:filename5_delete] == true

				Homework.set_filename(1, params[:filename1_file], record) if params[:filename1_file].present?
				Homework.set_filename(2, params[:filename2_file], record) if params[:filename2_file].present?
				Homework.set_filename(3, params[:filename3_file], record) if params[:filename3_file].present?
				Homework.set_filename(4, params[:filename4_file], record) if params[:filename4_file].present?
				Homework.set_filename(5, params[:filename5_file], record) if params[:filename5_file].present?

				record.verify_filenames
				record.set_options params[:options]

				json = record.attributes

				json['fs1'] = record.get_filename_size(1).to_f
				json['fs2'] = record.get_filename_size(2).to_f
				json['fs3'] = record.get_filename_size(3).to_f
				json['fs4'] = record.get_filename_size(4).to_f
				json['fs5'] = record.get_filename_size(5).to_f
					
				json['options'] = record.homework_options.order(option_order: :asc)

				render json: json, status: :ok
			end
		rescue => e
			Rails.logger.info "error message: #{e.message}"
			render json: {}, status: :unprocessable_entity
		end
	end

	def homework_delete
		begin

			if not @current_user.is_teacher_admin? and not teacher_belongs_to_classroom_and_course? params[:course_id], params[:classroom_id]
				render json: {}, status: :forbidden
				return
			end

			ActiveRecord::Base.transaction do
				record = Homework.where(id: params[:id], course_id: params[:course_id], classroom_id: params[:classroom_id]).take
				record.student_homeworks.destroy_all
				record.homework_options.destroy_all
				record.destroy!
			end
			render json: {}, status: :ok
		rescue => e
			puts e.message
			render json: {}, status: :unprocessable_entity
		end
	end

	def homeworks_set_ratings
		begin

			if not @current_user.is_teacher_admin? and not teacher_belongs_to_classroom_and_course? params[:course_id], params[:classroom_id]
				render json: {}, status: :forbidden
				return
			end

			homework = Homework.where(id: params[:homework_id], course_id: params[:course_id], classroom_id: params[:classroom_id]).take
			students = params[:students]
			ActiveRecord::Base.transaction do
				records = StudentHomework.set_ratings_for homework, students
				render json: {values: records}, status: :ok
			end
		rescue => e
			puts e.message
			render json: {}, status: :unprocessable_entity
		end
	end

	private

	def teacher_belongs_to_classroom_and_course?(course_id, classroom_id)
		puts "current user ID = #{@current_user.id}"
		puts "course_id = #{course_id}"
		puts "classroom_id = #{classroom_id}"
		TeacherCourseClassroom.where(course_id: course_id, classroom_id: classroom_id,teacher_id: @current_user.id).any?

	end

	def valid_params
		params.require(:course).permit(
			:id,
			:name,
			:is_pre_kinder,
			:is_kinder,
			:is_middle_school,
			:is_high_school,
		)
	end
	def valid_homework_params
		params.permit(
			:id,
			:title,
			:active,
			:of_type,
			:due_date,
			:content,
			:course_id,
			:classroom_id,
		)
	end

end
