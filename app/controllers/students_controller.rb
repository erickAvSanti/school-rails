class StudentsController < ApplicationController
  before_action :verify_if_user_is_teacher, only: [
    :for_teachers_list,
    :for_teachers_get_info,
    :for_teachers_create,
    :for_teachers_update,
    :for_teachers_delete,
  ]
  before_action :verify_if_user_teacher_can_create_update_delete, only: [
    :for_teachers_create,
    :for_teachers_update,
    :for_teachers_delete,
  ]
  def for_teachers_list
    order_by = '' 
    order_orientation = false
    id                = params[:id] if params[:id].present?
    order_by          = params[:order_by] if params[:order_by].present?
    order_orientation = params[:order_orientation]=='true' if params[:order_orientation].present?
    page              = params.has_key?(:page) ? params[:page].to_i : 1
    regs_x_page       = params.has_key?(:regs_x_page) ? params[:regs_x_page].to_i : REGS_X_PAGE
    word              = params[:word]
    is_active         = params[:is_active] rescue ''
    is_sex            = params[:is_sex] rescue ''
    records = []
    builder = User.students
    if word.present?
      builder = builder.where("fullname LIKE ?","%#{word}%")
    end
    if is_active.present?
      builder = builder.where("users.active = #{is_active}") if is_active.to_i == 1 or is_active.to_i == 0
    end
    if is_sex.present?
      builder = builder.where("users.sex = #{is_sex}") if is_sex.to_i == 1 or is_sex.to_i == 0
    end
    if order_by == 'fullname'
      builder = builder.order("users.fullname #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'onomastic'
      builder = builder.order("users.onomastic #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'sex'
      builder = builder.order("users.sex #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'active'
      builder = builder.order("users.active #{ order_orientation ? 'DESC' : 'ASC' }")
    end

    total = builder.count
    total_pages = ( total.to_f / regs_x_page.to_f ).floor + ( total % regs_x_page > 0 ? 1 : 0 )
    page = page > total_pages ? total_pages : page
    if total_pages>0
      offset = (page - 1) * regs_x_page
      builder.limit(regs_x_page).offset(offset).each do |record|
        obj = record.attributes
        #obj['access_history'] = record.user_history_requests.last( 20 )
        obj['access_history'] = UserHistoryRequest.get_for( record.id )
        obj['parents'] = record.parents
        records << obj
      end
    else
      page = 1
      total_pages = 0
    end
    render json: { 
      values: records, 
      total_records: total, 
      current_page: page, 
      total_pages: total_pages, 
      regs_x_page: regs_x_page, 
      order_by: order_by,
      order_orientation: order_orientation,  
    } , status: :ok
  end

  def for_teachers_get_info
    begin
      record = User.find params[:id]
      json = record.attributes
      json['parents'] = record.parents
      if record.picture_filename.present?
        directory = User::DIR_PROFILES
        if directory.present?
          filename_path = "#{directory}/#{record.picture_filename}"
          json['picture_filename_uri'] = "#{User::DIR_PROFILES_RELATIVE}/#{record.picture_filename}" if File.file? filename_path
        end
      end
      render json: json, status: :ok
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end
  def for_teachers_create
    begin
      pp = for_teachers_valid_params
      pp[:password_confirmation] = pp[:password] if pp[:password].present?
      record = User.new pp
      record.username = Faker::Internet.username(specifier: User::USERNAME_RANGE) if record.username.blank?
      if record.password.blank?
        new_password = Faker::Internet.password(min_length: User::PASSWORD_RANGE.first, max_length: User::PASSWORD_RANGE.last)
        record.password = new_password
        record.password_confirmation = new_password
      end 

      record.save!
      if params[:file].present?
        saved = User.set_picture(params[:file],record) 
        render json: {data: record,picture_saved: saved}, status: :ok
      else
        render json:{value: record}, status: :ok
      end
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end

  def for_teachers_update
    begin

      record = User.find params[:id]

      if @current_user.id != record.id and record.id == 1
        raise "You cant edit the first user"
      end
      pp = for_teachers_valid_params
      pp[:password_confirmation] = pp[:password] if pp[:password].present?
      record.update! pp
      if params[:file].present?
        saved = User.set_picture(params[:file],record) 
        render json: {data: record,picture_saved: saved}, status: :ok
      else
        render json:{data: record}, status: :ok
      end
    rescue => e
      puts "error => #{e.message}"
      record = nil
      render json: record, status: :bad_request
    end
  end

  def for_teachers_delete
    puts "delete params #{params}"
    begin
      record = User.find params[:id]
      record.destroy!
      if record.id != 1
        render json: {}, status: :ok
      else
        render json: {}, status: :not_acceptable
      end
    rescue => e
      puts "error => #{e.message}"
      render json: {}, status: :unprocessable_entity
    end
  end

  def full_profile
    begin
      raise "No eres un alumno" if not @current_user.is_student?
      
      record = @current_user
      obj = record.attributes
      obj.delete 'password_digest'
      obj.delete 'is_teacher_admin'
      render json:{ student: obj, parents: record.parents}, status: :ok
    rescue => e
      puts e.message
      render json:{}, status: :unprocessable_entity
    end
  end

  private
  def for_teachers_valid_params

    password = params[:password]
    username = params[:username]
    params[:password] = nil if params[:password].blank?
    params[:password_confirmation] = nil if params[:password_confirmation].blank?
    params[:document_number] = nil if params[:document_number].blank?
    if password.present? and (password.length < 10 or password.length > 30)
      raise "verifiy length of password"
    end
    permit_params = [
      :id,
      :fullname,
      :onomastic,
      :sex,
      :active,
      :document_number,
    ]



    if password.present?
      permit_params << :password
      permit_params << :password_confirmation
    end
    

    if username.present?
      permit_params << :username
    end

    puts "only permitted params before update or create"
    puts JSON.pretty_generate permit_params

    params.permit(permit_params)
  end

end
