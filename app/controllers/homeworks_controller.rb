class HomeworksController < ApplicationController
  REGS_X_PAGE = 20
  REGS_X_PAGE_MAX = 200
  def get_classroom_course_homeworks 
    begin
      classrooms = @current_user.classrooms
      homeworks_by_classrooms = []
      classrooms.each do |classroom|
        courses = classroom.courses
        homeworks_by_course = []
        courses.each do |course|
          homeworks_by_course << {
            course: course,
            homeworks: Homework.where(course_id: course.id, classroom_id: classroom.id)
          }
        end
        homeworks_by_classrooms << {
          classroom: classroom,
          courses: homeworks_by_course
        }
      end
      render json: {homeworks: homeworks_by_classrooms}, status: :ok
    rescue => e
      puts e.message
      render json: {}, status: :unprocessable_entity
    end
  end
  def get_homeworks
    order_by = '' 
    order_orientation = false
    id                = params[:id] if params[:id].present?
    timestamp_date        = params[:timestamp_date].to_date if params[:timestamp_date].present?
    timestamp_date_type   = params[:timestamp_date_type] if params[:timestamp_date_type].present?
    search_word       = params[:search_word] if params[:search_word].present?
    year              = params[:year].present? ? params[:year].to_i : DateTime.now.year
    course_id         = params[:course_id].to_i if params[:course_id].present?
    order_by          = params[:order_by] if params[:order_by].present?
    order_orientation = params[:order_orientation]=='true' if params[:order_orientation].present?
    page              = params.has_key?(:page) ? params[:page].to_i : 1
    regs_x_page       = params.has_key?(:regs_x_page) ? params[:regs_x_page].to_i : REGS_X_PAGE
    records = []
    of_type           = params[:of_type].to_i if params[:of_type].present?
    with_files        = params[:with_files].to_i if params[:with_files].present? and params[:with_files] != 'null'

    regs_x_page = REGS_X_PAGE_MAX if regs_x_page > REGS_X_PAGE_MAX

    builder = Homework.where(active: true)
    builder = builder.where(of_type: of_type) if of_type.present?
    if with_files == 1
      builder = builder.where(" homeworks.filename1 IS NOT NULL OR homeworks.filename2 IS NOT NULL OR homeworks.filename3 IS NOT NULL OR homeworks.filename4 IS NOT NULL OR homeworks.filename5 IS NOT NULL")
    elsif with_files == 0
      builder = builder.where(" homeworks.filename1 IS NULL AND homeworks.filename2 IS NULL AND homeworks.filename3 IS NULL AND homeworks.filename4 IS NULL AND homeworks.filename5 IS NULL")
    else
      #TODO
    end
    #builder = builder.where("DATE(homeworks.created_at) = '#{ timestamp_date.strftime("%Y-%m-%d") }' ") if ( timestamp_date.present? and timestamp_date_type == 'created_at' )
    #builder = builder.where("DATE(homeworks.updated_at) = '#{ timestamp_date.strftime("%Y-%m-%d") }' ") if ( timestamp_date.present? and timestamp_date_type == 'updated_at' )
    builder = builder.where("DATE(DATE_SUB(homeworks.created_at, INTERVAL 5 HOUR)) = '#{ timestamp_date.strftime("%Y-%m-%d") }' ") if ( timestamp_date.present? and timestamp_date_type == 'created_at' )
    builder = builder.where("DATE(DATE_SUB(homeworks.updated_at, INTERVAL 5 HOUR)) = '#{ timestamp_date.strftime("%Y-%m-%d") }' ") if ( timestamp_date.present? and timestamp_date_type == 'updated_at' )
    builder = builder.where("homeworks.title LIKE ? ","%#{search_word}%") if search_word.present?
    builder = builder.joins("JOIN classrooms_courses ON ( classrooms_courses.course_id = homeworks.course_id AND classrooms_courses.classroom_id = homeworks.classroom_id )")
    builder = builder.joins("JOIN classrooms ON classrooms.id = classrooms_courses.classroom_id")
    builder = builder.joins("JOIN courses ON courses.id = classrooms_courses.course_id")
    builder = builder.joins("JOIN classrooms_users ON classrooms.id = classrooms_users.classroom_id")
    builder = builder.joins("LEFT JOIN student_homeworks ON ( student_homeworks.homework_id = homeworks.id AND student_homeworks.student_id = #{@current_user.id} )")
    builder = builder.where(" classrooms_users.user_id = #{@current_user.id} ")
    
    builder = builder.where(" classrooms.academic_year_id = #{year} ") if year.present?
    builder = builder.where(" courses.id = #{course_id} ") if course_id.present?

    #builder = builder.joins("JOIN courses ON courses.id = homeworks.course_id")
    #builder = builder.joins("JOIN classrooms_courses ON classrooms_courses.course_id = courses.id")
    #builder = builder.joins("JOIN classrooms ON classrooms_courses.classroom_id = classrooms.id")
    
    if order_by == 'homework_title'
      builder = builder.order("homeworks.title #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'homework_rating'
      builder = builder.order("student_homeworks.rating #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'homework_due_date'
      builder = builder.order("homeworks.due_date #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'homework_updated_at'
      builder = builder.order("homeworks.updated_at #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'homework_created_at'
      builder = builder.order("homeworks.created_at #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'course_name'
      builder = builder.order("courses.name #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'classroom_title'
      builder = builder.order("classrooms.title #{ order_orientation ? 'ASC' : 'DESC' }")
    end
    if order_by == 'year'
      builder = builder.order("classrooms.academic_year_id #{ order_orientation ? 'ASC' : 'DESC' }")
    end

    courses_hash = {}
    classrooms_hash = {}

    total = builder.count
    total_pages = ( total.to_f / regs_x_page.to_f ).floor + ( total % regs_x_page > 0 ? 1 : 0 )
    page = page > total_pages ? total_pages : page
    if total_pages>0
      offset = (page - 1) * regs_x_page
      builder.limit(regs_x_page).offset(offset).each do |record|
        obj = record.attributes

        course    =  courses_hash.key?(record.course_id)        ? courses_hash[record.course_id]        : ( courses_hash[record.course_id]    = record.course )
        classroom =  classrooms_hash.key?(record.classroom_id)  ? classrooms_hash[record.classroom_id]  : ( classrooms_hash[record.course_id] = record.classroom )

        obj['course'] = course
        obj['classroom'] = classroom
        obj['rating'] = StudentHomework.where(student_id: @current_user.id, homework_id: record.id).take


        cc_valid_answer = record.homework_options.where(valid_answer: true).count
        obj['homework_options_radio_or_checkbox'] = cc_valid_answer > 0 ? ( cc_valid_answer > 1 ? 'checkbox' : 'radio' ) : nil

        homework_options_list = []
        record.homework_options.select(:id,:desc,:homework_id).each do |opt|
          homework_options_list << { text: opt.desc, value: opt.id } if cc_valid_answer == 1
          homework_options_list << { name: opt.desc, item: opt.id } if cc_valid_answer != 1
        end

        obj['fs1'] = record.get_filename_size(1).to_f
        obj['fs2'] = record.get_filename_size(2).to_f
        obj['fs3'] = record.get_filename_size(3).to_f
        obj['fs4'] = record.get_filename_size(4).to_f
        obj['fs5'] = record.get_filename_size(5).to_f

        obj['homework_options'] = {
          selected: (cc_valid_answer > 1 ? [] : nil),
          options: homework_options_list
        }
        
        records << obj
      end
    else
      page = 1
      total_pages = 0
    end
    render json: { 
      total_of_types: Homework.get_total_by_type_for_student_and_year(@current_user.id, year, course_id),
      values: records, 
      year: year, 
      course_id: course_id, 
      total_records: total, 
      current_page: page, 
      total_pages: total_pages, 
      regs_x_page: regs_x_page, 
      order_by: order_by,
      order_orientation: order_orientation,  
      academic_years: AcademicYear.to_options_ids,  
      courses: Course.to_options,  
    } , status: :ok
  end
end
