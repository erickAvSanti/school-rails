class WebsiteController < ApplicationController
  skip_before_action :authenticate_request, only:[:index]
  def index
    @images = Dir.glob("#{Rails.root}/app/assets/images/front/thumb_*.jpg").sort do |a,b|
      a <=> b
    end

    @images_thumb = []
    @images.each do |icon|
      filename = File.basename(icon)
      dim = FastImage.size(icon)
      @images_thumb << {
        dim: dim,
        filename: filename,
        url: "front/#{filename.gsub(/thumb_/,'')}",
        url_thumb: "front/#{filename}",
      }
    end

  end
end
