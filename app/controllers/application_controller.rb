class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  before_action :authenticate_request, except: [:test]
  before_action :inspect_url_request
  before_action :strip_params

  SELECT_OPTIONS_DEFAULT = 'Seleccionar'

  public

  def test
    render plain: 'test', status: :ok
  end
  
  private

  def inspect_url_request
    if defined?( @current_user )

      UserHistoryRequest.add_to_file( request, params, @current_user )
      #UserHistoryRequest.add_to_file( json )
      #UserHistoryRequest.add(json)

    end
  end

  def verify_if_user_is_teacher
    puts "Verifiyng if user is teacher"
    if not @current_user.is_teacher?
      render json: {}, status: :not_found
    end
  end

  def verify_if_user_teacher_can_create_update_delete
    puts "Verifiyng if user teacher can create, update or delete"
    puts "current user ID = #{@current_user.id}"
    if not @current_user.is_teacher_admin?
      render json: {}, status: :forbidden
    end
  end

  def strip_params
    if defined? params
      if params.kind_of? ActionController::Parameters
        params.each do |k,v|
          v.gsub!(/\s+/,' ') if v.kind_of? String
          v.strip! if v.kind_of? String
          if v.kind_of? Array
            v.each do |vv|
              vv.gsub!(/\s+/,' ') if vv.kind_of? String
              vv.strip! if vv.kind_of? String
            end
          elsif v.kind_of? Hash
            v.each do |k,vv|
              v[k].gsub!(/\s+/,' ') if vv.kind_of? String
              v[k].strip! if vv.kind_of? String
            end
          end
        end
      end
    end
  end
  def authenticate_request
    @current_user = AuthorizeApiRequest.call(request.headers).result
    @user_token = nil
    if @current_user.blank?
      puts "user blank"
      render json: { error: 'User no found',headers: request.headers['Authorization'] }, status: 401
    elsif !@current_user.active
      puts "user inactive"
      render json: { error: 'User inactived' }, status: 401
    else
      token_code = request.headers['Authorization'].split(' ').last
      @user_token = UserToken.where(user_id: @current_user.id).where(token: token_code).take
      if @user_token.blank?
        puts "user token blank"
        render json: { error: 'Token code not found' }, status: 401
      else 
        if not @user_token.active
          puts "user token inactive"
          render json: { error: 'Token code inactive' }, status: 401
        end
      end 
    end 
  end
end
