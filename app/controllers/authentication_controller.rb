class AuthenticationController < ApplicationController
 	skip_before_action :authenticate_request, only: [:authenticate]

 	def logout
    begin 
      @user_token.active = false
      @user_token.save!
    rescue
      Rails.logger.info "Logout warn = #{@user_token.errors.to_s}"
    end
 		render json: { success: true }, status: :ok
 	end
 	def authenticate
    username = params[:username]
    password = params[:password]

    if ((username.blank? or !User::USERNAME_RANGE.include?(username.length) ) or (password.blank? or !User::PASSWORD_RANGE.include?(password.length)))
      render json: { error: ['undefined inputs'] }, status: :unauthorized
      return
    end

		command = AuthenticateUser.call(username,password)

		if command.success? and command.user_active
      tk = command.result
      ut = UserToken.new
      ut.token = tk
      ut.user_id = command.user_id
      ut.device_type = !params[:device_type]
      ut.ip = request.remote_ip
      begin
        ut.save!
        render json: { 
          auth_token: tk,
          fullname: command.fullname,
          type: command.user_type,
          user_info: command.user_info,
        }
      rescue => e
        puts e.message
        render json: { error: ['cant create token!'] }, status: :unauthorized
      end
		else
      puts "command not success or not user active"
      errors = command.errors
      errors = 'user-inactive' if !command.user_active
      render json: { error: errors }, status: :unauthorized
		end
 	end
end