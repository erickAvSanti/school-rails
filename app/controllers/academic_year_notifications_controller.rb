class AcademicYearNotificationsController < ApplicationController
  before_action :verify_if_user_is_teacher, only: [
    :for_teachers_get_notifications,
    :for_teachers_create,
    :for_teachers_update,
    :for_teachers_delete,
  ]

  REGS_X_PAGE = 20
  REGS_X_PAGE_MAX = 200

  public 

  def get_student_notifications
    begin

      order_by = '' 
      order_orientation = false
      id                = params[:id] if params[:id].present?
      order_by          = params[:order_by] if params[:order_by].present?
      order_orientation = params[:order_orientation]=='true' if params[:order_orientation].present?
      page              = params.has_key?(:page) ? params[:page].to_i : 1
      regs_x_page       = params.has_key?(:regs_x_page) ? params[:regs_x_page].to_i : REGS_X_PAGE
      regs_x_page       = REGS_X_PAGE_MAX if regs_x_page > REGS_X_PAGE_MAX

      records = []
      builder = AcademicYearNotification.where(academic_year_id: id, is_public: true)
      if order_by == 'notification_title'
        builder = builder.order("academic_year_notifications.title #{ order_orientation ? 'ASC' : 'DESC' }")
      end
      if order_by == 'notification_created_at'
        builder = builder.order("academic_year_notifications.created_at #{ order_orientation ? 'ASC' : 'DESC' }")
      end
      if order_by == 'notification_updated_at'
        builder = builder.order("academic_year_notifications.updated_at #{ order_orientation ? 'ASC' : 'DESC' }")
      end

      total = builder.count
      total_pages = ( total.to_f / regs_x_page.to_f ).floor + ( total % regs_x_page > 0 ? 1 : 0 )
      page = page > total_pages ? total_pages : page
      if total_pages>0
        offset = (page - 1) * regs_x_page
        builder.limit(regs_x_page).offset(offset).each do |record|
          obj = record.attributes
          obj['teacher'] = record.teacher.attributes.slice('id','picture_filename','fullname')
          records << obj
        end
      else
        page = 1
        total_pages = 0
      end
      render json: { 
        values: records, 
        total_records: total, 
        current_page: page, 
        total_pages: total_pages, 
        regs_x_page: regs_x_page, 
        order_by: order_by,
        order_orientation: order_orientation,  
      } , status: :ok
    rescue => e
      puts e.message
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_get_notifications
    begin

      order_by = '' 
      order_orientation = false
      id                = params[:id] if params[:id].present?
      order_by          = params[:order_by] if params[:order_by].present?
      order_orientation = params[:order_orientation]=='true' if params[:order_orientation].present?
      page              = params.has_key?(:page) ? params[:page].to_i : 1
      regs_x_page       = params.has_key?(:regs_x_page) ? params[:regs_x_page].to_i : REGS_X_PAGE
      regs_x_page       = REGS_X_PAGE_MAX if regs_x_page > REGS_X_PAGE_MAX

      records = []
      builder = AcademicYearNotification.where(academic_year_id: id)
      if order_by == 'notification_title'
        builder = builder.order("academic_year_notifications.title #{ order_orientation ? 'ASC' : 'DESC' }")
      end
      if order_by == 'notification_is_public'
        builder = builder.order("academic_year_notifications.is_public #{ order_orientation ? 'ASC' : 'DESC' }")
      end
      if order_by == 'notification_created_at'
        builder = builder.order("academic_year_notifications.created_at #{ order_orientation ? 'ASC' : 'DESC' }")
      end
      if order_by == 'notification_updated_at'
        builder = builder.order("academic_year_notifications.updated_at #{ order_orientation ? 'ASC' : 'DESC' }")
      end

      total = builder.count
      total_pages = ( total.to_f / regs_x_page.to_f ).floor + ( total % regs_x_page > 0 ? 1 : 0 )
      page = page > total_pages ? total_pages : page
      if total_pages>0
        offset = (page - 1) * regs_x_page
        builder.limit(regs_x_page).offset(offset).each do |record|
          obj = record.attributes
          records << obj
        end
      else
        page = 1
        total_pages = 0
      end
      render json: { 
        values: records, 
        total_records: total, 
        current_page: page, 
        total_pages: total_pages, 
        regs_x_page: regs_x_page, 
        order_by: order_by,
        order_orientation: order_orientation,  
        im_admin: @current_user.is_teacher_admin,
      } , status: :ok
    rescue => e
      puts e.message
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_create
    begin
      puts params

      ActiveRecord::Base::transaction do 
        academic_year_id = params[:academic_year_id].to_i
        raise "year out of now" if academic_year_id > DateTime.now.year
        AcademicYear.find_or_create_by(id: academic_year_id)
        record = AcademicYearNotification.new valid_params
        record.teacher_id = @current_user.id
        record.save!

        #AcademicYearNotification.set_filename(1, params[:filename1_file], record) if params[:filename1_file].present?
        #AcademicYearNotification.set_filename(2, params[:filename2_file], record) if params[:filename2_file].present?
        #AcademicYearNotification.set_filename(3, params[:filename3_file], record) if params[:filename3_file].present?

        #record.verify_filenames

        json = record.attributes
        render json: json, status: :ok
      end
    rescue => e
      puts e.message
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_update
    begin
      if not @current_user.is_teacher_admin? and not teacher_belongs_to_notification? params[:id], params[:academic_year_id]
        render json: {}, status: :forbidden
        return
      end

      puts params
      ActiveRecord::Base.transaction do
        
        record = AcademicYearNotification.where(id: params[:id], academic_year_id: params[:academic_year_id]).take 
        record.update! valid_params
        
        record.remove_filename(1) if params[:filename1_delete] == "true" or params[:filename1_delete] == true
        record.remove_filename(2) if params[:filename2_delete] == "true" or params[:filename2_delete] == true
        record.remove_filename(3) if params[:filename3_delete] == "true" or params[:filename3_delete] == true
        record.remove_filename(4) if params[:filename4_delete] == "true" or params[:filename4_delete] == true
        record.remove_filename(5) if params[:filename5_delete] == "true" or params[:filename5_delete] == true
        
        AcademicYearNotification.set_filename(1, params[:filename1_file], record) if params[:filename1_file].present?
        AcademicYearNotification.set_filename(2, params[:filename2_file], record) if params[:filename2_file].present?
        AcademicYearNotification.set_filename(3, params[:filename3_file], record) if params[:filename3_file].present?
        AcademicYearNotification.set_filename(4, params[:filename4_file], record) if params[:filename4_file].present?
        AcademicYearNotification.set_filename(5, params[:filename5_file], record) if params[:filename5_file].present?
        
        record.verify_filenames

        json = record.attributes

        render json: json, status: :ok
      end
    rescue => e
      puts e.message
      render json: {}, status: :unprocessable_entity
    end
  end

  def for_teachers_delete
    puts "delete params #{params}"
    begin
      record = AcademicYearNotification.find params[:id]
      record.destroy
      render json: {}, status: :ok
    rescue => e
      puts "error => #{e.message}"
      render json: {}, status: :not_found
    end
  end

  private 

  def teacher_belongs_to_notification? notification_id, academic_year_id
    AcademicYearNotification.where(id: notification_id, teacher_id: @current_user.id, academic_year_id: academic_year_id).any?
  end

  def valid_params
    params.permit(
      :id,
      :title,
      :is_public,
      :desc,
      :teacher_id,
      :academic_year_id,
    )
  end

end
