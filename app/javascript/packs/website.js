
jQuery(function(evt){
	jQuery(".block-icon").click(function(){
		const obj = jQuery(this)
		const src = obj.attr("image-src")
		console.log(src)
		jQuery('#image_view').modal('show')
		jQuery("#image_view_block img").attr("src",src)
	})
	jQuery(".intranet_steps").click(function(){
		const obj = jQuery(this)
		const src = obj.attr("src")
		console.log(src)
		jQuery('#image_view').modal('show')
		jQuery("#image_view_block img").attr("src",src)
	})
})

jQuery(function(){
	document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	    anchor.addEventListener('click', function (e) {
	        e.preventDefault()

	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	            behavior: 'smooth'
	        })
	    })
	})
})