class AuthenticateUser
  prepend SimpleCommand

  def initialize(username, password)
    @username = username
    @password = password
    @u = nil
  end

  def fullname
    "#{@u.fullname}" if @u.present? 
  end

  def user_active
    @u.present? and @u.active 
  end

  def user_type
    @u.present? and @u.user_type 
  end

  def user_id
    @u.id if @u.present?
  end

  def user_info
    rr = @u.attributes
    rr.delete 'password_digest'
    rr if rr.present?
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  attr_accessor :username, :password

  def user
    puts "find user by username #{username}"
    user = User.find_by_username(username)
    @u = user
    puts "authenticate user with password = #{password}"
    return user if user && user.authenticate(password)
    puts "error x1"
    errors.add :user_authentication, 'invalid credentials'
    nil
  end
end