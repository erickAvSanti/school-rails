module TeachersHelper

  MAX_SEED = 160
  FILE_SEED = Rails.root.join('storage','teachers_seed.txt')
  def self.generate_seed max = MAX_SEED
    data = []
    return if User.teachers.any?
    from = User.maximum(:id).next
    to = from + max
    from.upto(to).each do |number| 
      password = Faker::Internet.password(min_length: 10)
      json = {
        id: number,
        user_type: false,
        username: Faker::Internet.username,
        fullname: Faker::Name.name,
        password: password,
        password_confirmation: password,
        active: Faker::Boolean.boolean,
        sex: Faker::Boolean.boolean,
        document_number: Faker::IDNumber.brazilian_citizen_number,
        onomastic: Faker::Date.between(from: 5.years.ago, to: 1.years.ago),
      }
      data << json
    end
    File.open(FILE_SEED,'wb'){|f| f.write JSON.pretty_generate(data)}
  end

  def self.create_data_from_seed_generated
    data = File.read FILE_SEED, encoding: 'UTF-8'
    if data.blank?
      puts "data empty, file #{FILE_SEED}"
      return
    end
    json = JSON.parse data
    json.each do |el|
      breaking = false
      record = User.new el
      begin
        record.save!(validate: false)
      rescue => e
        puts e.message
        breaking = true
      end
      break if breaking
    end
  end
end
