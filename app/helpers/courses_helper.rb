module CoursesHelper

  MAX_SEED = 27
  FILE_SEED = Rails.root.join('storage','courses_seed.txt')
  def self.generate_seed max = MAX_SEED
    data = []

    id = 1
    arr = [
      'matemática',
      'historia universal',
      'historia del perú',
      'geometría',
      'trigonometría',
      'comunicación',
      'religión',
      'inglés',
    ]
    counter = 1
    arr.each do |nn| 
      json = {
        id: counter,
        name: nn.upcase,
        is_pre_kinder: Faker::Boolean.boolean,
        is_kinder: Faker::Boolean.boolean,
        is_middle_school: Faker::Boolean.boolean,
        is_high_school: Faker::Boolean.boolean,
      }
      counter += 1
      data << json
    end
    File.open(FILE_SEED,'wb'){|f| f.write JSON.pretty_generate(data)}
  end

  def self.create_data_from_seed_generated
    data = File.read FILE_SEED, encoding: 'UTF-8'
    if data.blank?
      puts "data empty, file #{FILE_SEED}"
      return
    end
    Course.destroy_all
    json = JSON.parse data
    json.each do |el|
      breaking = false
      record = Course.new el
      begin
        record.save!(validate: false)
      rescue => e
        puts e.message
        breaking = true
      end
      break if breaking
    end
  end
end
