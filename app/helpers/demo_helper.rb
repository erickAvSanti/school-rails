module DemoHelper
  def self.rand_string(from = 0,to = 40)
    (from...to).map { ('a'..'z').to_a[rand(26)] }.join
  end

  def self.clearAppData
    Homework.destroy_all
    Course.destroy_all
    TeacherCourseClassroom.destroy_all
    Classroom.destroy_all
  end
end