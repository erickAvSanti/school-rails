module ParentsHelper

  MAX_SEED = 55
  FILE_SEED = Rails.root.join('storage','parents_seed.txt')
  def self.generate_seed max = MAX_SEED
    data = []
    1.upto(max).each do |number| 

      parent_type = [
        'PADRE',
        'MADRE',
        'HERMANO',
        'HERMANA',
        'PRIMO',
        'PRIMA',
        'TIO',
        'TIA',
        'SOBRINO',
        'SOBRINA',
        'ABUELO',
        'ABUELA',
      ].sample
      json = {
        id: number,
        fullname: Faker::Name.name,
        document_number: Faker::IDNumber.brazilian_citizen_number,
        parent_type: parent_type,
        onomastic: Faker::Date.between(from: 15.years.ago, to: 30.years.ago),
        phone_number1: Faker::PhoneNumber.cell_phone,
        phone_number2: Faker::PhoneNumber.cell_phone,
        email1: Faker::Internet.free_email,
        email2: Faker::Internet.free_email,
        address1: Faker::Address.street_address,
        address2: Faker::Address.street_address,
      }
      data << json
    end
    File.open(FILE_SEED,'wb'){|f| f.write JSON.pretty_generate(data)}
  end

  def self.create_data_from_seed_generated
    data = File.read FILE_SEED, encoding: 'UTF-8'
    if data.blank?
      puts "data empty, file #{FILE_SEED}"
      return
    end
    json = JSON.parse data
    json.each do |el|
      breaking = false
      record = Parent.new el
      begin
        record.save!(validate: false)
      rescue => e
        puts e.message
        breaking = true
      end
      break if breaking
    end
  end
end
