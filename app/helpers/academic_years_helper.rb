module AcademicYearsHelper

  MAX_SEED_CLASSROOMS = 200
  MAX_SEED_CLASSROOMS_STUDENTS = 400
  MAX_SEED_CLASSROOMS_COURSES = 200
  MAX_SEED_CLASSROOMS_COURSES_TEACHERS = 20
  MAX_SEED_CLASSROOMS_COURSES_HOMEWORKS = 20
  FILE_SEED_CLASSROOMS = Rails.root.join('storage','classrooms_seed.txt')
  FILE_SEED_CLASSROOMS_STUDENTS = Rails.root.join('storage','classrooms_students_seed.txt')
  def self.generate_seed_classrooms max = MAX_SEED_CLASSROOMS
    data = []
    1.upto(max).each do |number| 
      json = {
        id: number,
        title: Faker::Lorem.paragraph(sentence_count: 1),
        description: Faker::Lorem.words(number: 20).join(" "),
        academic_year_id: AcademicYear.order(Arel.sql("RAND()")).take.id
      }
      data << json
    end
    File.open(FILE_SEED_CLASSROOMS,'wb'){|f| f.write JSON.pretty_generate(data)}
  end

  def self.create_data_from_seed_generated_classrooms
    data = File.read FILE_SEED_CLASSROOMS, encoding: 'UTF-8'
    if data.blank?
      puts "data empty, file #{FILE_SEED_CLASSROOMS}"
      return
    end
    json = JSON.parse data
    json.each do |el|
      breaking = false
      record = Classroom.new el
      begin
        record.save!(validate: false)
      rescue => e
        puts e.message
        breaking = true
      end
      break if breaking
    end
  end





  def self.generate_seed_classrooms_students max = MAX_SEED_CLASSROOMS_STUDENTS
    1.upto(max).each do |number| 
      max_students = Faker::Number.within(range: 8..15)
      students = []
      1.upto(max_students).each do |pos|
        students << User.students.order(Arel.sql("RAND()")).take
      end
      classroom = Classroom.order(Arel.sql('RAND()')).take
      if not classroom.users.any?
        classroom.users << students
      end
    end
  end






  def self.generate_seed_classrooms_courses max = MAX_SEED_CLASSROOMS_COURSES
    1.upto(max).each do |number| 
      max_courses = Faker::Number.within(range: 4..7)
      courses = []
      1.upto(max_courses).each do |pos|
        courses << Course.order(Arel.sql("RAND()")).take
      end
      courses.uniq!
      classroom = Classroom.order(Arel.sql('RAND()')).take
      if not classroom.courses.any?
        classroom.courses << courses
      end
    end
  end






  def self.generate_seed_classrooms_courses_teachers max = MAX_SEED_CLASSROOMS_COURSES_TEACHERS

    AcademicYear.all.each do |ay|
      ay.classrooms.each do |classroom|
        classroom.courses.each do |course|
          tcc = TeacherCourseClassroom.where(course_id: course.id, classroom_id: classroom.id)
          if not tcc.any?
            max_teachers = Faker::Number.within(range: 1..3)
            teachers = []
            1.upto(max_teachers).each do |pos|
              teacher = User.teachers.order(Arel.sql("RAND()")).take
              teachers << teacher if teacher.present?
              puts "teacher not found" if teacher.blank?
            end
            teachers = teachers.uniq { |tt| tt.id }
            teachers.each do |tt|
              props = { 
                course_id: course.id, 
                classroom_id: classroom.id, 
                teacher_id: tt.id 
              }
              tcc = TeacherCourseClassroom.new props
              begin
                tcc.save!
              rescue => e
                puts e.message
              end
            end
          end
        end
      end
    end
  end






  def self.generate_seed_classrooms_courses_homeworks max = MAX_SEED_CLASSROOMS_COURSES_HOMEWORKS

    AcademicYear.all.each do |ay|
      ay.classrooms.each do |classroom|
        classroom.courses.each do |course|
          homeworks = Homework.where(classroom_id: classroom.id, course_id: course.id)
          if not homeworks.any?
            max_homeworks = Faker::Number.within(range: 8..45)
            1.upto(max_homeworks).each do |number|
              props = {
                title: Faker::Lorem.words(number: 5).join(' '),
                content: Faker::Lorem.words(number: 20).join(' '),
                due_date: Faker::Time.between(from: DateTime.now, to: DateTime.now + 2.month),
              }
              record = Homework.new props
              record.classroom_id = classroom.id
              record.course_id = course.id
              begin
                record.save!
              rescue => e
              end
            end
          end
        end
      end
    end
  end
end
