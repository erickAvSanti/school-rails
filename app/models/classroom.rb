class Classroom < ApplicationRecord
  has_and_belongs_to_many :users
  has_and_belongs_to_many :courses
  belongs_to :academic_year, foreign_key: :academic_year_id

  has_many :teacher_course_classrooms, foreign_key: :classroom_id

  has_many :homeworks
  has_many :classroom_schedules, foreign_key: :classroom_id

  before_destroy :can_destroy?

  private

  def can_destroy?
    #students of students is scope filter
    if users.any?
      errors[:base] << "Este aula / salón tiene alumnos"
      throw :abort
    end
    if courses.any?
      errors[:base] << "Este aula / salón tiene cursos"
      throw :abort
    end
  end

  public 

  def set_students_with_ids ids 
    users.clear
    values = User.students.where("id IN (?)", ids )
    users << values if values.count > 0
  end

  def set_courses_with_ids ids
    if ids.blank?
      courses.each do |course|
        TeacherCourseClassroom.where(course_id: course.id, classroom_id: id).destroy_all
      end
    else
      current_ids = courses.pluck('id')
      ids_to_delete = current_ids - ids
      TeacherCourseClassroom.where(classroom_id: id).where("course_id in (?)",ids_to_delete).destroy_all if ids_to_delete.present?
      ids_to_insert = ids - current_ids
      if ids_to_insert.present?
        values = Course.where("id IN (?)", ids_to_insert )
        courses << values if values.count > 0
      end
    end
  end

  def courses_with_teachers
    arr_courses = []
    courses.each do |course|
      obj = course.attributes
      teacher_course_classrooms = TeacherCourseClassroom.where(course_id: course.id, classroom_id: id)
      obj['teachers'] = User.teachers.where("id in (?)",teacher_course_classrooms.pluck('teacher_id')).order(fullname: :asc)
      arr_courses << obj
    end if courses.present?
    arr_courses
  end

  def courses_with_teachers_and_total_homeworks
    arr_courses = []
    courses.each do |course|
      obj = course.attributes
      obj['total_homeworks'] = Homework.where(course_id: course.id, classroom_id: id).count
      teacher_course_classrooms = TeacherCourseClassroom.where(course_id: course.id, classroom_id: id)
      schedule = ClassroomSchedule.where(course_id: course.id, classroom_id: id).take
      if schedule.present?
        schedule = schedule.attributes.slice("id","schedule_information")
      else
        schedule = {}
      end
      obj['schedule'] = schedule
      obj['teachers'] = User.teachers.where("id in (?)",teacher_course_classrooms.pluck('teacher_id')).order(fullname: :asc)
      arr_courses << obj
    end if courses.present?
    arr_courses
  end

  def self.set_teachers_for_courses( classroom_record, teachers)
    if teachers.present?
      teachers.each do |json|
        course_id     = json['course_id']
        teachers_ids  = json['teachers_ids']
        if teachers_ids.present?
          current_teacher_ids = TeacherCourseClassroom.where(classroom_id: classroom_record.id, course_id: course_id).pluck('teacher_id')
          puts "current teacher IDs = #{current_teacher_ids.to_s}"
          puts "new teacher IDs = #{teachers_ids.to_s}"
          teacher_ids_to_delete = current_teacher_ids - teachers_ids
          puts "current minus existing IDs = #{teacher_ids_to_delete.to_s}"
          TeacherCourseClassroom.where(classroom_id: classroom_record.id, course_id: course_id).where("teacher_id in (?)",teacher_ids_to_delete).destroy_all if teacher_ids_to_delete.present?
          teacher_ids_to_assign = teachers_ids - current_teacher_ids
          teacher_ids_to_assign.each do |teacher_id|
            json = {
              classroom_id: classroom_record.id, 
              course_id: course_id, 
              teacher_id: teacher_id
            }
            record = TeacherCourseClassroom.new json
            record.save!
          end if teacher_ids_to_assign.present?
        else
          TeacherCourseClassroom.where(classroom_id: classroom_record.id, course_id: course_id).destroy_all
        end
      end 
    else
      TeacherCourseClassroom.where(classroom_id: classroom_record.id).destroy_all
    end
  end

end
