class UserHistoryRequest < ApplicationRecord
  belongs_to :user, foreign_key: :user_id
  DIR = Rails.root.join('storage','history_requests')
  public 
  def self.add json
    record = UserHistoryRequest.new json
    record.save!
  end

  def self.add_to_file( request, params, current_user )

    json = {
      url: request.original_url,
      method: request.method,
      subdomain: params[:subdomain],
      controller: params[:controller],
      action: params[:action],
      created_at: DateTime.now.to_s,
    }

    json_map = {
      user_id: current_user.id,
      json: json.to_json.to_s,
    }

    FileUtils.mkdir_p( DIR ) if not Dir.exist? DIR
    file = "#{DIR}/#{json_map[:user_id]}.txt"
    Rails.logger.info " >>>>>>>>>>> Writing to file = #{file}"
    print " >>>>>>>>>>> Writing to file = #{file}"
    File.open(file,"a") {|f| f.write( "#{json_map[:json]}\n" ) }
  end

  def self.get_for user_id 
    file = "#{DIR}/#{user_id}.txt"
    return [] if not File.file? file
    data = `tail -20 #{file}`
    arr = data.split(/\n/)
    if arr.class == Array
      arr.each_with_index do |line, idx|
        json = JSON.parse line
        arr[idx] = {
          json: line,
          created_at: json['created_at'],
          updated_at: json['created_at'],
        }
      end
    else 
      arr = []
    end
    arr
  end

end
