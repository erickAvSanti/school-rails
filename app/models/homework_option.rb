class HomeworkOption < ApplicationRecord
  belongs_to :homework, foreign_key: :homework_id
end
