class AcademicYearNotification < ApplicationRecord

  belongs_to :academic_year, foreign_key: :academic_year_id
  belongs_to :teacher, foreign_key: :teacher_id, class_name: 'User'

  DIR_NOTIFICATIONS = Rails.root.join('public','notifications')


  ACCEPTED_FORMATS = [
    '.doc',
    '.docx',
    '.pdf',
    '.txt',
    '.ppt',
    '.pptx',
    '.jpg',
    '.jpeg',
    '.png',
    '.mpeg4',
    '.mp4',
    '.mp3',
    '.wav',
    '.ogg',
    '.flv',
  ]
  after_destroy :after_destroy_event

  private

  def after_destroy_event
    if folder.present?
      directory = "#{DIR_NOTIFICATIONS}/#{academic_year_id.to_s}"
      if filename1.present?
        file_path = "#{directory}/#{filename1}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
      if filename2.present?
        file_path = "#{directory}/#{filename2}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
      if filename3.present?
        file_path = "#{directory}/#{filename3}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
      if filename4.present?
        file_path = "#{directory}/#{filename4}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
      if filename5.present?
        file_path = "#{directory}/#{filename5}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
    end
  end

  public
  def remove_filename(cond = 1)


    directory = "#{DIR_NOTIFICATIONS}/#{academic_year_id.to_s}"
    FileUtils.mkdir_p(directory,mode: 0777) if not Dir.exist? directory

    filename_path = "#{directory}/#{filename1}" if cond == 1 and filename1.present?
    filename_path = "#{directory}/#{filename2}" if cond == 2 and filename2.present?
    filename_path = "#{directory}/#{filename3}" if cond == 3 and filename3.present?
    filename_path = "#{directory}/#{filename4}" if cond == 4 and filename4.present?
    filename_path = "#{directory}/#{filename5}" if cond == 5 and filename5.present?
    
    if filename_path.present? and File.file? filename_path
      
      update!({filename1: nil}) if cond == 1
      update!({filename2: nil}) if cond == 2
      update!({filename3: nil}) if cond == 3
      update!({filename4: nil}) if cond == 4
      update!({filename5: nil}) if cond == 5
      puts "eliminando archivo de tipo #{cond}"
      

      FileUtils.remove_file filename_path
      return true

    else
      puts "Ningún archivo por eliminar"
    end
  end


  def verify_filenames

    
    directory = "#{DIR_NOTIFICATIONS}/#{academic_year_id.to_s}"
    FileUtils.mkdir_p(directory,mode: 0777) if not Dir.exist? directory
    
    filename_path = "#{directory}/#{filename1}"
    update!({filename1: nil}) if not File.file? filename_path
    
    filename_path = "#{directory}/#{filename2}"
    update!({filename2: nil}) if not File.file? filename_path
    
    filename_path = "#{directory}/#{filename3}"
    update!({filename3: nil}) if not File.file? filename_path
    
    filename_path = "#{directory}/#{filename4}"
    update!({filename4: nil}) if not File.file? filename_path
    
    filename_path = "#{directory}/#{filename5}"
    update!({filename5: nil}) if not File.file? filename_path
    
  end

  def self.set_filename(cond = 1,file, record)
    return if not file.kind_of? ActionDispatch::Http::UploadedFile

    directory = "#{DIR_NOTIFICATIONS}/#{record.academic_year_id.to_s}"
    FileUtils.mkdir_p(directory,mode: 0777) if not Dir.exist? directory

    cond = cond.to_i
    puts "procesando arhivo #{file}"
    puts "procesando record = #{record}"
    raise "condition undefined" if cond != 1 and cond != 2 and cond != 3
    filename1_rev = record.filename1 if cond == 1
    filename2_rev = record.filename2 if cond == 2
    filename3_rev = record.filename3 if cond == 3
    filename4_rev = record.filename4 if cond == 4
    filename5_rev = record.filename5 if cond == 5
    
    puts "procesando extensión = #{File.extname(file.original_filename).downcase}"
    if ACCEPTED_FORMATS.include? File.extname(file.original_filename).downcase
      puts "formato de archivo aceptado"
      filename_hash = "#{DemoHelper.rand_string(0,65)}#{File.extname(file.original_filename)}"

      filename_path = "#{directory}/#{filename_hash}"

      filename_path_prev = "#{directory}/#{filename1_rev}" if cond == 1 and filename1_rev.present?
      filename_path_prev = "#{directory}/#{filename2_rev}" if cond == 2 and filename2_rev.present?
      filename_path_prev = "#{directory}/#{filename3_rev}" if cond == 3 and filename3_rev.present?
      filename_path_prev = "#{directory}/#{filename4_rev}" if cond == 4 and filename4_rev.present?
      filename_path_prev = "#{directory}/#{filename5_rev}" if cond == 5 and filename5_rev.present?

      FileUtils.cp file.tempfile.path, filename_path

      if File.file? filename_path
        puts "#{filename_path} archivo temporal movido para la notificación"
        record.filename1 = filename_hash if cond == 1
        record.filename2 = filename_hash if cond == 2
        record.filename3 = filename_hash if cond == 3
        record.filename4 = filename_hash if cond == 4
        record.filename5 = filename_hash if cond == 5
        begin
          record.save!
          if filename_path_prev.present? and File.file? filename_path_prev
            FileUtils.remove_file(filename_path_prev)
            puts "#{filename_path_prev} eliminado"
          end
          puts "datos de tarea y su archivo actualizado"
          return true
        rescue => e
          puts "error type = #{e.class}"
          puts e.full_message
          puts "#{filename_path} eliminando porque no se pudo actualizar datos de la notificación"
          FileUtils.remove_file(filename_path)
          raise "Ocurrió un error cuando se intentaba guardar y asignar un archivo"
        end
      end
    else
      puts "Archivo no incluido en los formatos aceptados"
    end
    false
  end
end
