require 'favicon'
class AppSetting < ApplicationRecord
  FILENAMES = [
    'android-icon-144x144',
    'android-icon-192x192',
    'android-icon-36x36',
    'android-icon-48x48',
    'android-icon-72x72',
    'android-icon-96x96',

    'apple-icon-114x114',
    'apple-icon-120x120',
    'apple-icon-140x140',
    'apple-icon-152x152',
    'apple-icon-180x180',
    'apple-icon-57x57',
    'apple-icon-60x60',
    'apple-icon-72x72',
    'apple-icon-76x76',
  ]

  FAVICONS = [
    'favicon-16x16',
    'favicon-32x32',
    'favicon-96x96',
  ]

  MS_ICONS = [
    'ms-icon-144x144',
    'ms-icon-150x150',
    'ms-icon-310x310',
    'ms-icon-70x70',
  ]

  def self.get_from_key(kk, def_str = '')
    AppSetting.where(kk: kk).take.vv
  rescue => e
    puts e.message
    def_str
  end

  def self.makeFavicons(src = nil)
    return nil if src.blank?
    ext = File.extname(src).downcase
    FILENAMES.each do |ff|
      matches = ff.match /\-(\w+)$/
      if matches.present?
        size = matches.to_a[1]
        dst = "#{Rails.root.join('public',ff)}#{ext}"
        AppSetting.cropSquare( src, dst, size )
      end
    end
    FAVICONS.each do |ff|
      matches = ff.match /\-(\w+)$/
      if matches.present?
        size = matches.to_a[1]
        dst = "#{Rails.root.join('public',ff)}#{ext}"
        AppSetting.cropSquare( src, dst, size )
      end
    end
    MS_ICONS.each do |ff|
      matches = ff.match /\-(\w+)$/
      if matches.present?
        size = matches.to_a[1]
        dst = "#{Rails.root.join('public',ff)}#{ext}"
        AppSetting.cropSquare( src, dst, size )
      end
    end
    dst = "#{Rails.root.join('public','apple-icon-precomposed')}#{ext}"
    AppSetting.cropSquare( src, dst, "192x192" )
    dst = "#{Rails.root.join('public','apple-icon')}#{ext}"
    AppSetting.cropSquare( src, dst, "192x192" )
    #Favicon.png_to_favicon( src, Rails.root.join('public', 'favicon.ico' ).to_s )
    nil
  end

  def self.cropSquare(src, dest = nil, new_size = "400x400")
    dest = src if dest.blank?
    image = MiniMagick::Image.open(src)
    puts File.basename(src)
    dim = image.dimensions
    width = dim[0]
    height = dim[1]
    orientation = ( width < height ) ? 'v' : 'h'
    size = ( width < height ) ? width : height
    if orientation == 'h'
      image.crop "#{size}x#{size}+#{(width - size)/2}+0"
    else
      image.crop "#{size}x#{size}+0+#{(height - size)/2}"
    end
    image.resize new_size
    #thumbnail = filename_path.gsub(/(\w+\.\w+)$/,"thumb_\\1")
    image.write(dest)
    nil
  end
end
