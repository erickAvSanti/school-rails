class StudentHomework < ApplicationRecord
  belongs_to :homework, foreign_key: :homework_id

  public

  def self.set_ratings_for homework, students
    
    ratings = homework.student_homeworks
    current_students_ids = ratings.pluck(:student_id)
    new_students_ids = []
    new_students_ids_set = {}
    students.each do |student|
      new_students_ids_set[student['id'].to_i] = student
      new_students_ids << student['id']
    end

    puts "current students IDS = #{current_students_ids}"
    puts "new students IDS = #{new_students_ids}"

    students_to_delete = current_students_ids - new_students_ids
    students_to_create_or_update = new_students_ids

    puts "students IDS to delete #{students_to_delete}"
    puts "students IDS to create or update #{students_to_create_or_update}"

    students_to_delete.each do |id|
      StudentHomework.where(homework_id: homework.id, student_id: id).destroy
    end

    students_to_create_or_update.each do |id|
      json = new_students_ids_set[id.to_i]
      record = StudentHomework.where(homework_id: homework.id, student_id: id).take
      record = StudentHomework.new if record.blank?
      record.homework_id = homework.id
      record.student_id = id
      record.rating = json['rating'].present? ? json['rating'].to_f : 0

      raise "rating out of [0,20]" if record.rating > 20 or record.rating < 0

      (record.details = json['details']).gsub!(/\s+/,' ') if json['details'].present?
      record.save!
    end
    homework.student_homeworks

  end

end
