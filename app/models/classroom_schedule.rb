class ClassroomSchedule < ApplicationRecord
  belongs_to :classroom

  public

  def self.set_for(classroom_id, course_id, schedule_information)
    record = ClassroomSchedule.where(classroom_id: classroom_id, course_id: course_id).take
    record = ClassroomSchedule.new if record.blank?
    record.classroom_id = classroom_id
    record.course_id = course_id
    record.schedule_information = schedule_information
    record.save!
    record
  rescue => e
    puts e.message
    nil
  end

end
