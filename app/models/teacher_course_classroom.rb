class TeacherCourseClassroom < ApplicationRecord
  belongs_to :classroom, foreign_key: :classroom_id
end
