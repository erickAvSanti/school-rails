class Parent < ApplicationRecord
  has_and_belongs_to_many :users

  before_destroy :can_destroy?

  private

  def can_destroy?
    if users.any?
      errors[:base] << "Este pariente tiene usuarios asignados"
      throw :abort
    end
  end

  public

  def self.clean_parameters form_parameters

    attributes = Parent.new.attributes
    pp = []
    attributes.each { |k,v| pp << k }
    form_parameters = form_parameters.permit(pp).to_h
    form_parameters.each do |k,v|
      form_parameters[k] = nil if v.blank?
    end
    form_parameters
  end
end
