class AcademicYear < ApplicationRecord
  has_many :classrooms, foreign_key: :academic_year_id
  has_many :academic_year_notifications, foreign_key: :academic_year_id


  before_destroy :can_destroy?
  
  private

  def can_destroy?
    if classrooms.any?
      errors[:base] << "Este año académico tiene salones asignados"
      throw :abort
    end
    if academic_year_notifications.any?
      errors[:base] << "Este año académico tiene notificaciones asignadas"
      throw :abort
    end
  end

  public

  def self.to_options_ids
    arr = []
    AcademicYear.where("id <= #{(DateTime.now + 1.year).year}").order(id: :desc).pluck(:id).each do |year|
      arr << {
        text: year,
        value: year,
      }
    end
    another_vv = {text: DateTime.now.year, value: DateTime.now.year}
    arr << another_vv
    arr.sort_by! { |vv| vv[:value] }
    arr.uniq!
    arr.reverse!
    arr = [{text: 'Seleccione un año',value: nil,}] + arr
    arr
  end

  def self.new_with_year academic_year_id
    begin
      record = AcademicYear.find academic_year_id
    rescue => e
      puts e.message
      record = AcademicYear.new({description: "año académico #{academic_year_id}"})
      record.id = academic_year_id
      record.save!
      record
    end
  end

end
