class Homework < ApplicationRecord
  belongs_to :course
  belongs_to :classroom
  has_many :student_homeworks, foreign_key: :homework_id
  has_many :homework_options, foreign_key: :homework_id

  DIR_HOMEWORKS = Rails.root.join('public','homeworks')

  ACCEPTED_FORMATS = [
    '.doc',
    '.docx',
    '.pdf',
    '.txt',
    '.ppt',
    '.pptx',
    '.jpg',
    '.jpeg',
    '.png',
    '.mpeg4',
    '.mp4',
    '.mp3',
    '.wav',
    '.ogg',
    '.flv',
  ]

  before_destroy :can_destroy?
  after_destroy :after_destroy_event

  private

  def after_destroy_event
    if folder.present?
      directory = "#{DIR_HOMEWORKS}/#{folder}"
      if filename1.present?
        file_path = "#{directory}/#{filename1}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
      if filename2.present?
        file_path = "#{directory}/#{filename2}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
      if filename3.present?
        file_path = "#{directory}/#{filename3}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
      if filename4.present?
        file_path = "#{directory}/#{filename4}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
      if filename5.present?
        file_path = "#{directory}/#{filename5}"
        FileUtils.remove_file(file_path) if File.file? file_path
      end
    end
  end

  def can_destroy?
    if student_homeworks.any?
      errors[:base] << "Esta tarea tiene calificaciones asignadas"
      throw :abort
    end
    if homework_options.any?
      errors[:base] << "Esta tarea tiene opciones asignadas"
      throw :abort
    end
  end

  public 

  def set_options(options_str = nil)
    return if options_str.blank?
    options = JSON.parse options_str
    if options.blank?
      homework_options.destroy_all
    else
      options.each_index do |idx|
        current_option = options[idx]
        current_option['option_order'] = 0
        if current_option['id'].to_i <= 0
          current_option.delete 'id'
          current_option['homework_id'] = id
          (options[idx] = HomeworkOption.new(current_option)).save!
        else
          current_option['homework_id'] = id
          current_option.delete 'updated_at'
          current_option.delete 'created_at'
          (registered_option = HomeworkOption.find(current_option['id'])).update!(current_option)
          options[idx] = registered_option
        end
      end

      options.each_index do |idx|
        options[idx].update!({option_order: (idx+1)})
      end

      homework_options.each do |ho|
        found = false
        options.each do |opt|
          if opt.id == ho.id
            found = true
            break
          end
        end
        ho.destroy if not found
      end

    end

  end

  def verify_filenames

    if folder.present?
      directory = "#{DIR_HOMEWORKS}/#{folder}"

      filename_path = "#{directory}/#{filename1}"
      update!({filename1: nil}) if not File.file? filename_path
      
      filename_path = "#{directory}/#{filename2}"
      update!({filename2: nil}) if not File.file? filename_path
      
      filename_path = "#{directory}/#{filename3}"
      update!({filename3: nil}) if not File.file? filename_path
      
      filename_path = "#{directory}/#{filename4}"
      update!({filename4: nil}) if not File.file? filename_path
      
      filename_path = "#{directory}/#{filename5}"
      update!({filename5: nil}) if not File.file? filename_path

    end

  end

  def get_filename_path(cond = 1)
    return "#{DIR_HOMEWORKS}/#{folder}/#{filename1}" if folder.present? and filename1.present? and cond == 1
    return "#{DIR_HOMEWORKS}/#{folder}/#{filename2}" if folder.present? and filename2.present? and cond == 2
    return "#{DIR_HOMEWORKS}/#{folder}/#{filename3}" if folder.present? and filename3.present? and cond == 3
    return "#{DIR_HOMEWORKS}/#{folder}/#{filename4}" if folder.present? and filename4.present? and cond == 4
    return "#{DIR_HOMEWORKS}/#{folder}/#{filename5}" if folder.present? and filename5.present? and cond == 5
    nil
  end

  def get_filename_size(cond = 1)
    ff = get_filename_path cond
    return '%.2f' % (File.size(ff).to_f / 2**20) if ff.present?
    0
  end

  def remove_filename(cond = 1)
    if folder.present?

      directory = "#{DIR_HOMEWORKS}/#{folder}"

      filename_path = "#{directory}/#{filename1}" if cond == 1 and filename1.present?
      filename_path = "#{directory}/#{filename2}" if cond == 2 and filename2.present?
      filename_path = "#{directory}/#{filename3}" if cond == 3 and filename3.present?
      filename_path = "#{directory}/#{filename4}" if cond == 4 and filename4.present?
      filename_path = "#{directory}/#{filename5}" if cond == 5 and filename5.present?
      
      if filename_path.present? and File.file? filename_path
        
        update!({filename1: nil}) if cond == 1
        update!({filename2: nil}) if cond == 2
        update!({filename3: nil}) if cond == 3
        update!({filename4: nil}) if cond == 4
        update!({filename5: nil}) if cond == 5
        puts "eliminando archivo de tipo #{cond}"
        

        FileUtils.remove_file filename_path
        return true

      else
        puts "Ningún archivo por eliminar"
      end

    end
    false
  end

  def self.get_total_by_type(classroom_id, course_id)
    return nil if classroom_id.blank? or course_id.blank?
    sql = "
      select 
        homeworks.of_type, 
        count(homeworks.id) 
      from 
        homeworks 
      where 
        homeworks.classroom_id = #{classroom_id.to_i} 
        AND homeworks.course_id = #{course_id.to_i} 
        AND homeworks.active = 1
      group by homeworks.of_type
      "
    builder = ActiveRecord::Base.connection.execute(sql)
    builder.to_a
  end

  def self.get_total_by_type_for_student_and_year(user_id, year, course_id)

    sql = "

      select distinct 
        homeworks.of_type, 
        count(homeworks.id) 
      from 
        homeworks  
        JOIN classrooms_users ON ( classrooms_users.classroom_id = homeworks.classroom_id AND classrooms_users.user_id = #{ user_id } ) 
        JOIN classrooms ON classrooms.id = classrooms_users.classroom_id 
        #{ course_id.present? ? "JOIN classrooms_courses ON ( classrooms_courses.classroom_id = classrooms.id AND classrooms_courses.course_id = #{ course_id } )" : "" } 
      where 1  
        AND classrooms.academic_year_id = #{year} 
        #{ course_id.present? ? "AND homeworks.course_id = #{ course_id }" : "" }
        AND homeworks.active = 1
      group by homeworks.of_type 
    "
    builder = ActiveRecord::Base.connection.execute(sql)
    builder.to_a
  end

  def self.set_filename(cond = 1,file, record)
    return if not file.kind_of? ActionDispatch::Http::UploadedFile
    cond = cond.to_i
    puts "procesando arhivo #{file}"
    puts "procesando record = #{record}"
    raise "condition undefined" if cond != 1 and cond != 2 and cond != 3 and cond != 4 and cond != 5
    filename1_rev = record.filename1 if cond == 1
    filename2_rev = record.filename2 if cond == 2
    filename3_rev = record.filename3 if cond == 3
    filename4_rev = record.filename4 if cond == 4
    filename5_rev = record.filename5 if cond == 5
    
    puts "procesando extensión = #{File.extname(file.original_filename).downcase}"
    if ACCEPTED_FORMATS.include? File.extname(file.original_filename).downcase
      puts "formato de archivo aceptado"
      filename_hash = "#{DemoHelper.rand_string}#{File.extname(file.original_filename)}"
    
      directory = "#{DIR_HOMEWORKS}/#{record.folder}"
      FileUtils.mkdir_p(directory,mode: 0777) if not Dir.exist? directory
      puts "directorio de tarea = #{directory}"

      filename_path = "#{directory}/#{filename_hash}"

      filename_path_prev = "#{directory}/#{filename1_rev}" if cond == 1 and filename1_rev.present?
      filename_path_prev = "#{directory}/#{filename2_rev}" if cond == 2 and filename2_rev.present?
      filename_path_prev = "#{directory}/#{filename3_rev}" if cond == 3 and filename3_rev.present?
      filename_path_prev = "#{directory}/#{filename4_rev}" if cond == 4 and filename4_rev.present?
      filename_path_prev = "#{directory}/#{filename5_rev}" if cond == 5 and filename5_rev.present?

      FileUtils.cp file.tempfile.path, filename_path

      if File.file? filename_path
        puts "#{filename_path} archivo temporal movido para la tarea"
        record.filename1 = filename_hash if cond == 1
        record.filename2 = filename_hash if cond == 2
        record.filename3 = filename_hash if cond == 3
        record.filename4 = filename_hash if cond == 4
        record.filename5 = filename_hash if cond == 5
        begin
          record.save!
          if filename_path_prev.present? and File.file? filename_path_prev
            FileUtils.remove_file(filename_path_prev)
            puts "#{filename_path_prev} eliminado"
          end
          puts "datos de tarea y su archivo actualizado"
          return true
        rescue => e
          puts "error type = #{e.class}"
          puts e.full_message
          puts "#{filename_path} eliminando porque no se pudo actualizar datos de la tarea"
          FileUtils.remove_file(filename_path)
          raise "Ocurrió un error cuando se intentaba guardar y asignar un archivo"
        end
      end
    else
      puts "Archivo no incluido en los formatos aceptados"
    end
    false
  end

end
