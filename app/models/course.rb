class Course < ApplicationRecord
  has_and_belongs_to_many :classrooms

  has_many :homeworks

  before_destroy :can_destroy?

  def self.to_options
    arr = []
    arr << {
      text: 'Todos los cursos',
      value: nil,
    }
    Course.order(name: :asc).each do |cc|
      arr << {
        text: cc.name.upcase,
        value: cc.id,
      }
    end
    arr
  end

  private

  def can_destroy?
    #if classrooms.any?
    #  errors[:base] << "Este curso tiene un aula / salón asignado"
    #  throw :abort
    #end

    if homeworks.any?
      errors[:base] << "Este curso tiene tareas"
      throw :abort
    end
  end
end
