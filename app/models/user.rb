class User < ApplicationRecord
  DIR_PROFILES_RELATIVE = 'profiles'
  DIR_PROFILES = Rails.root.join('public',DIR_PROFILES_RELATIVE)

  USERNAME_RANGE = 10..30
  PASSWORD_RANGE = 8..30
  DOCUMENT_SIZE_RANGE = 8..15

  has_secure_password

  has_many :user_tokens, foreign_key: :user_id
  has_many :user_history_requests, foreign_key: :user_id
  has_many :academic_year_notifications, foreign_key: :teacher_id
  has_and_belongs_to_many :parents
  has_and_belongs_to_many :classrooms
  validates :username, length: { in: USERNAME_RANGE }, presence: { message: "Indique su usuario de acceso" }, uniqueness: {message: "Este usuario ya existe", case_sensitive: true}
  validates :document_number, length: { in: DOCUMENT_SIZE_RANGE }, allow_nil: true, uniqueness: {message: "Este documento ya existe", case_sensitive: true}
  
  scope :students, -> { where(user_type: true) }
  scope :teachers, -> { where(user_type: false) }
  scope :teachers_admins, -> { where(user_type: false, is_teacher_admin: true) }

  before_destroy :can_destroy?

  private

  def can_destroy?
    if parents.any?
      errors[:base] << "Este usuario tiene parientes"
      throw :abort
    end
    if classrooms.any?
      errors[:base] << "Este usuario tiene aulas o salones asignado"
      throw :abort
    end
    if academic_year_notifications.any?
      errors[:base] << "Este usuario tiene notificaciones de año académico"
      throw :abort
    end
  end

  public

  def is_student?
    user_type == true
  end
  def is_teacher?
    user_type == false
  end
  def make_teacher
    update!({user_type: false})
  end
  def make_student
    update!({user_type: true})
  end

  def set_new_password pwd
    raise "invalid password size" if (pwd.blank? or not PASSWORD_RANGE.include? pwd.size)
    update!({password: pwd, password_confirmation: pwd }) 
  end

  def set_new_credentials usr, pwd
    raise "invalid password size" if (pwd.blank? or not PASSWORD_RANGE.include? pwd.size)
    update!({username: usr,password: pwd, password_confirmation: pwd }) 
  end


  def self.set_picture(file,record)
    picture_filename_prev = record.picture_filename
    if file.content_type == 'image/png' or file.content_type == 'image/jpeg' or file.content_type == 'image/jpg'
      
      filename_hash = "#{DemoHelper.rand_string}#{File.extname(file.original_filename)}"
      
      directory = DIR_PROFILES
      FileUtils.mkdir_p(directory,mode: 0777) if not Dir.exist? directory

      filename_path = "#{directory}/#{filename_hash}"
      filename_path_prev = "#{directory}/#{picture_filename_prev}"

      FileUtils.cp file.tempfile.path, filename_path

      if File.file? filename_path
        puts "#{filename_path} archivo temporal movido para el usuario"
        record.picture_filename = filename_hash
        begin
          record.save!
          if File.file? filename_path_prev
            FileUtils.remove_file(filename_path_prev)
            puts "#{filename_path_prev} eliminado"
          end
          puts "datos de usuario y su archivo actualizado"
          return true
        rescue => e
          puts e.message
          puts "#{filename_path} eliminando porque no se pudo actualizar datos de usuario"
          FileUtils.remove_file(filename_path)
          return false
        end
      end
    end
    false
  end
end
