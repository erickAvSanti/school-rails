#!/bin/bash
cd "$(dirname "$0")"
zip -r "../school-rails-backup-$(date +'%d_%m_%y__%H_%M_%S').zip" . -x "./public/homeworks/*" "./public/notifications/*"

LINE=$(awk '/DATABASE/{print $2}' ".env.production")
arrIN=(${LINE//=/ })
DB_NAME=${arrIN[1]}
echo ${DB_NAME}

mysqldump -uroot -proot $DB_NAME > "../${DB_NAME}___$(date +'%d_%m_%y__%H_%M_%S').sql"